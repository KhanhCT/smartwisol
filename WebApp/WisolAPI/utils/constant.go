package utils

const (
	AUTHORIZATION = "Authorization"
	DEBUG = false
	TIMEOUT = 2
	YYYY_MM_DD = "2006-01-02"
	YYYY_MM_DD_HH_MM_SS = "2006-01-02 15:04:05"
	BASE_URL = "/api/v0.1/"
	STATUS_ERROR = "Error"
	STATUS_SUCCESS = "Success"
)
