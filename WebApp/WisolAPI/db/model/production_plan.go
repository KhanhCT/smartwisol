package model
import (
	"time"
	"fmt"

	"wisol/utils"
)

type ProductionPlan struct {
	WorkingDate time.Time `json:"WorkingDate" db:"WorkingDate"`
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	WorkPlaceID int `json:"WorkPlaceID" db:"WorkPlaceID"`
	ProductID int `json:"ProductID" db:"ProductID"`
	OrderedQty int `json:"OrderedQty" db:"OrderedQty"`
	GoodProdQty int `json:"GoodProdQty" db:"GoodProdQty"`
	NGProdQty int `json:"NGProdQty" db:"NGProdQty"`
	StartTime time.Time  `json:"StartTime" db:"StartTime"`
	StopTime time.Time `json:"StopTime" db:"StopTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type ProductionPlanDTO struct {
	WorkingDate string `json:"WorkingDate" db:"WorkingDate"`
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	WorkPlaceID int `json:"WorkPlaceID" db:"WorkPlaceID"`
	ProductID int `json:"ProductID" db:"ProductID"`
	ProductName string `json:"ProductName" db:"ProductName"`
	OrderedQty int `json:"OrderedQty" db:"OrderedQty"`
	GoodProdQty int `json:"GoodProdQty" db:"GoodProdQty"`
	NGProdQty int `json:"NGProdQty" db:"NGProdQty"`
	StartTime string  `json:"StartTime" db:"StartTime"`
	StopTime string `json:"StopTime" db:"StopTime"`
	CycleTime float32 `json:"CycleTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
	FactoryName string `json:"FactoryName" db:"ProductName"`
	LineCode string `json:"LineCode" db:"LineCode"`
	ShiftName string `json:"ShiftName" db:"ShiftName"`
	WorkPlaceName string `json:"Code" db:"Code"`
}
type GoalObj struct  {
	ID int `json:"ID"`
	WorkPlaceName string `json:"01"`
	ProductName string `json:"02"`
	OrderedQty int `json:"03"`
	GoodProdQty int `json:"04"`	
	Goal string `json:"05"`
}

func (dto ProductionPlanDTO) DTO2Model() (ProductionPlan, error) {
	workingDate, err := time.Parse(utils.YYYY_MM_DD, dto.WorkingDate)
	if err != nil {
		return ProductionPlan{}, err
	}
	
	startTime, err := time.Parse(utils.YYYY_MM_DD_HH_MM_SS, dto.StartTime)
	if err != nil {
		fmt.Println(err.Error())
		return ProductionPlan{}, err
	}
	stopTime, err := time.Parse(utils.YYYY_MM_DD_HH_MM_SS, dto.StopTime)
	if err != nil {
		return ProductionPlan{}, err
	}
	model := ProductionPlan{
		WorkingDate: workingDate,
		FactoryID: dto.FactoryID,
		LineID: dto.LineID,
		ShiftID: dto.ShiftID,
		ProductID: dto.ProductID,
		OrderedQty: dto.OrderedQty,
		GoodProdQty: dto.GoodProdQty,
		NGProdQty: dto.NGProdQty, 
		WorkPlaceID: dto.WorkPlaceID,
		StartTime: startTime,
		StopTime: stopTime,
		Disabled: dto.Disabled,
	}
	return model, nil
}
