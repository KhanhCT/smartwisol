package model

import(
	"time"
	"database/sql"
)

type Account struct {
	ID int `json:"ID" db:"db"`
	Username string `json:"Username" db:"Username" xml:"username"`
	Password string `json:"Password" db:"Password" xml:"password"`
	Fullname  sql.NullString  `json:"FullName" db:"Ffullname"`
	WorkerID sql.NullInt64 `json:"WorkerID" db:"worker_id"`
	RoleID int `json:"RoleID" db:"role_id"`
	GroupID sql.NullInt64 `json:"GroupID" db:"group_id"`
	CreatedDate time.Time `json:"CreatedDate" db:"created_date"`
	UpdatedDate time.Time `json:"UpdatedDate" db:"updated"`
	LastLogin time.Time `json:"LastLogin" db:"LastLogin"`
	Disabled bool `json:"Disabled" db:"disable"`
}

type AccountDTO struct {
	ID int `json:"ID" db:"db"`
	Username string `json:"username" db:"Username" xml:"username"`
	Password string `json:"password" db:"Password" xml:"password"`
	Fullname string  `json:"fullname" db:"fullname"`
	WorkerID  int `json:"worker_id" db:"worker_id"`
	RoleID int `json:"role_id" db:"role_id"`
	GroupID int `json:"group_id" db:"group_id"`
	CreatedDate time.Time `json:"created_date" db:"created_date"`
	UpdatedDate time.Time `json:"updated_date" db:"updated"`
	LastLogin time.Time `json:"last_login" db:"LastLogin"`
	Disabled bool `json:"disabled" db:"disable"`
}

func (model Account) Model2DTO() AccountDTO{
	dto := AccountDTO{
		ID: model.ID,
		Username: model.Username,
		Password: model.Password,
		Fullname: model.Fullname.String, 
		WorkerID: int(model.WorkerID.Int64),
		RoleID: model.RoleID,
		GroupID: int(model.GroupID.Int64),
		CreatedDate: model.CreatedDate,
		UpdatedDate: model.UpdatedDate,
		LastLogin: model.LastLogin,
		Disabled: model.Disabled,
	}
	return dto
}

func (dto AccountDTO) DTO2Model() Account{
	model := Account{
		ID: dto.ID,
		Username: dto.Username,
		Password: dto.Password,
		Fullname: sql.NullString {
			String : dto.Fullname,
			Valid : dto.Fullname != "",
		}, 
		WorkerID: sql.NullInt64 {
			Int64 : int64(dto.WorkerID),
			Valid : dto.WorkerID != 0,
		},
		RoleID: dto.RoleID,
		GroupID: sql.NullInt64 {
			Int64 : int64(dto.GroupID),
			Valid : dto.GroupID != 0,
		},
		CreatedDate: dto.CreatedDate,
		UpdatedDate: time.Now(),
		LastLogin: time.Now(),
		Disabled: dto.Disabled,
	}
	return model
}