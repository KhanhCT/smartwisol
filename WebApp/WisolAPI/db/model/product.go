package model
import (
	"database/sql"
)

type Product struct {
	ProductID int `json:"ProductID" db:"ProductID"`
	Barcode sql.NullString `json:"Barcode" db:"Barcode"`
	ProductName string `json:"ProductName" db:"ProductName"`
	UnitID sql.NullInt64 `json:"UnitID" db:"UnitID"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type ProductDTO struct {
	ProductID int `json:"ProductID" db:"ProductID"`
	Barcode string `json:"Barcode" db:"Barcode"`
	ProductName string `json:"ProductName" db:"ProductName"`
	UnitID int `json:"UnitID" db:"UnitID"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

func (prod Product) Model2DTO() ProductDTO {
	dto := ProductDTO{
		ProductID: prod.ProductID,
		Barcode: prod.Barcode.String,
		ProductName: prod.ProductName,
		UnitID: int(prod.UnitID.Int64),
		Disabled: prod.Disabled,
	}
	return dto
}