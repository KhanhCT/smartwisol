package model

type Status struct {
	ID string `json:"ID`
	Name string `json: "Name"`
	Color string `json: "Color"`
}