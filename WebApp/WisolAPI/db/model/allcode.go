package model

import (
	"database/sql"
)

type AllCode struct{
	ID int `json:"ID" db:"id"`
	Name string `json:"Name" db:"name"`
	Type string `json:"Type" db:"Type"`
	CodeIDX string `json:"CodeIDX" db:"CodeIDX"`
	Value string `json:"Value" db:"value"`
	Description sql.NullString `json:"Description" db:"description"`
	Modified bool `json:"Modified" db:"modified"`
	Disabled bool `json:"Disabled" db:"disabled"`
}	