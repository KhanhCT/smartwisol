package model

import (
	"database/sql"
	"time"
)

type Customer struct {
	ID int  `json:"ID" db:"ID"`
	Code string `json:"Code" db:"Code"`
	FirstName string  `json:"FirstName" db:"FirstName"`
	LastName string  `json:"LastName" db:"LastName"`
	Gender sql.NullString  `json:"Gender" db:"Gender"`
	JobTitle sql.NullString  `json:"JobTitle" db:"JobTitle"`
	Address sql.NullString  `json:"Address" db:"Address"`
	PhoneNum sql.NullString  `json:"PhoneNum" db:"PhoneNum"`
	Age sql.NullInt64  `json:"Age" db:"Age"`
	Nationality sql.NullString `json:"Nationality" db:"Nationality"`
	CreatedDate time.Time   `json:"CreatedDate"  db:"CreatedDate"` 
	DeptID int  `json:"DeptID" db:"DeptID"`
	CardID int  `json:"CardID" db:"CardID"`
	UserID int `json:"UserID" db:"UserID"`
	TypeID sql.NullString `json:"TypeID" db:"TypeID"`
	PPID sql.NullInt64 `json:"PPID" db:"PPID"`
	ParentID sql.NullInt64 `json:"ParentID" db:"ParentID"`
	Balance sql.NullFloat64 `json:"Balance" db:"Balance"`
	DiscPrice sql.NullFloat64 `json:"DiscPrice" db:"DiscPrice"`
	Contact sql.NullString  `json:"Contact" db:"Contact"`
	Email sql.NullString  `json:"Email" db:"Email"`
	Remark sql.NullString  `json:"Remark" db:"Remark"`
	Status bool  `json:"status" db:"Status"` 
}


type CustomerDTO struct {
	ID int  `json:"ID" db:"ID"`
	Code string `json:"Code" db:"Code"`
	FirstName string  `json:"FirstName" db:"FirstName"`
	LastName string  `json:"LastName" db:"LastName"`
	Gender string  `json:"Gender" db:"Gender"`
	JobTitle string  `json:"JobTitle" db:"JobTitle"`
	Address string  `json:"Address" db:"Address"`
	PhoneNum string  `json:"PhoneNum" db:"PhoneNum"`
	Age int  `json:"Age" db:"Age"`
	Nationality string `json:"Nationality" db:"Nationality"`
	CreatedDate string   `json:"CreatedDate"  db:"CreatedDate"` 
	DeptID int  `json:"DeptID" db:"DeptID"`
	CardID int  `json:"CardID" db:"CardID"`
	UserID int `json:"UserID" db:"UserID"`
	TypeID string `json:"TypeID" db:"TypeID"`
	PPID int `json:"PPID" db:"PPID"`
	ParentID int `json:"ParentID" db:"ParentID"`
	Balance float64 `json:"Balance" db:"Balance"`
	DiscPrice float64 `json:"DiscPrice" db:"DiscPrice"`
	Contact string  `json:"Contact" db:"Contact"`
	Email string  `json:"Email" db:"Email"`
	Remark string  `json:"Remark" db:"Remark"`
	Status bool  `json:"Status" db:"Status"` 
}


func (cus Customer) Model2DTO() CustomerDTO{
	dto := CustomerDTO{
		ID: cus.ID,
		FirstName: cus.FirstName,
		LastName: cus.LastName,
		Gender: cus.Gender.String,
		JobTitle: cus.JobTitle.String,
		Address: cus.Address.String,
		PhoneNum: cus.PhoneNum.String,
		Age: int(cus.Age.Int64),
		Nationality: cus.Nationality.String,
		//CreatedDate: cus.CreatedDate,
		Status: cus.Status,
		DeptID: cus.DeptID,
		CardID: cus.CardID,
		UserID: cus.UserID,
		TypeID: cus.TypeID.String,
	}
	return dto
}

func (cus CustomerDTO) DTO2Model() Customer{
	m := Customer{
		ID: cus.ID,
		FirstName: cus.FirstName,
		LastName: cus.LastName,
		Gender :sql.NullString {
			String : cus.Gender,
			Valid : cus.Gender != "",
		},
		JobTitle :sql.NullString {
			String : cus.JobTitle,
			Valid : cus.JobTitle != "",
		},
		Address :sql.NullString {
			String : cus.Address,
			Valid : cus.Address != "",
		},
		PhoneNum :sql.NullString {
			String : cus.PhoneNum,
			Valid : cus.PhoneNum != "",
		},
		Age :sql.NullInt64 {
			Int64 : int64(cus.Age),
			Valid : cus.Age != 0,
		},
		Nationality :sql.NullString {
			String : cus.Nationality,
			Valid : cus.Nationality != "",
		},
		//CreatedDate: cus.CreatedDate,
		Status: cus.Status,
		DeptID: cus.DeptID,
		CardID: cus.CardID,
		UserID: cus.UserID,
		TypeID :sql.NullString {
			String : cus.TypeID,
			Valid : cus.TypeID != "",
		},
	}
	return m
}
