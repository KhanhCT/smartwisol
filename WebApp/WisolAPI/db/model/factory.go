package model
import (
	"database/sql"
)

type Factory struct {
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	FactoryCode string `json:"FactoryCode" db:"FactoryCode"`
	FactoryName sql.NullString `json:"FactoryName" db:"FactoryName"`
	ShiftQty int `json:"ShiftQty" db:"ShiftQty"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type FactoryDTO struct {
	FactoryID int `json:"FactoryID" db:"factory_id"`
	FactoryCode string `json:"FactoryCode" db:"factory_code"`
	FactoryName string `json:"FactoryName" db:"factory_name"`
	ShiftQty int `json:"ShiftQty" db:"shift_qty"`
	Disabled bool `json:"Disabled" db:"disabled"`
}

func (f Factory)Model2DTO() FactoryDTO{
	dto := FactoryDTO{
		FactoryID: f.FactoryID,
		FactoryCode: f.FactoryCode,
		FactoryName: f.FactoryName.String,
		ShiftQty: f.ShiftQty,
		Disabled: f.Disabled,
	}
	return dto
}
