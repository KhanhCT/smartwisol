package model
import (
	"database/sql"
)
type ProductionLine struct {
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	LineCode string `json:"LineCode" db:"LineCode"`
	Description sql.NullString `json:"Description" db:"Description"`
	WorkPlaceQty int `json:"WorkPlaceQty" db:"WorkPlaceQty"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type ProductionLineDTO struct {
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	LineCode string `json:"LineCode" db:"LineCode"`
	Description string `json:"Description" db:"Description"`
	WorkPlaceQty int `json:"WorkPlaceQty" db:"WorkPlaceQty"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

func (pl ProductionLine) Model2DTO() ProductionLineDTO{
	dto := ProductionLineDTO{
		FactoryID: pl.FactoryID,
		LineID: pl.LineID,
		LineCode: pl.LineCode,
		Description: pl.Description.String,
		WorkPlaceQty: pl.WorkPlaceQty,
		Disabled: pl.Disabled,
	}
	return dto
}