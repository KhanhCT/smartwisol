package model

import (
	"time"
	"database/sql"
	"wisol/utils"
)

type Role struct {
	ID int   `json:"id"  db:"id"`
	Name string  `json:"name"  db:"name"`
	CreatedDate time.Time `json:"created_date"  db:"created_date"`
	Description sql.NullString `json:"description"  db:"description"`
	UserID sql.NullInt64 `json:"user_id"  db:"user_id"`
	Disabled  bool `json:"disabled" db:"disable"`
}
type RoleDTO struct {
	ID int   `json:"id"  db:"id"`
	Name string  `json:"name"  db:"name"`
	CreatedDate string `json:"created_date"  db:"created_date"`
	Description string `json:"description"  db:"description"`
	UserID int `json:"user_id"  db:"user_id"`
	Disabled  bool `json:"disabled" db:"disable"`
}

func (rt Role) Model2DTO() RoleDTO{
	dto := RoleDTO{
		ID: rt.ID,
		Name: rt.Name,
		CreatedDate: rt.CreatedDate.Format(utils.YYYY_MM_DD),
		Description: rt.Description.String,
		UserID: int(rt.UserID.Int64),
		Disabled: rt.Disabled,
	}
	return dto
}

func (rt RoleDTO) DTO2Model() Role{
	role := Role{
		Name : rt.Name,
		CreatedDate: time.Now(),
		Description :sql.NullString {
			String : rt.Description,
			Valid : rt.Description != "",
		},
		UserID :sql.NullInt64 {
			Int64 : int64(rt.UserID),
			Valid : rt.UserID != 0,
		},
		Disabled: rt.Disabled,
	}
	return role
}