package model
import(
	"database/sql"
)
type Device struct {
	DeviceID string `json:"DeviceID" db:"DeviceID"`
	DeviceName string `json:"DeviceName" db:"DeviceName"`
	FactoryID sql.NullInt64 `json:"FactoryID" db:"FactoryID"`
	LineID sql.NullInt64 `json:"LineID" db:"LineID"`
	WorkPlaceID sql.NullInt64 `json:"WorkPlaceID" db:"WorkPlaceID"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type DeviceDTO struct {
	DeviceID string `json:"DeviceID" db:"DeviceID"`
	DeviceName string `json:"DeviceName" db:"DeviceName"`
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	WorkPlaceID int `json:"WorkPlaceID" db:"WorkPlaceID"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

func (d Device) Model2DTO() DeviceDTO{
	dto := DeviceDTO{
		DeviceID: d.DeviceID,
		DeviceName: d.DeviceName,
		FactoryID: int(d.FactoryID.Int64),
		LineID: int(d.LineID.Int64),
		WorkPlaceID: int(d.WorkPlaceID.Int64),
		Disabled: d.Disabled,
	}
	return dto
}