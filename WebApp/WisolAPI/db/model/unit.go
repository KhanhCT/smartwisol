package model

import (
)

type Unit struct {
	ID int `json:"unit_id" db:"unit_id"`
	UnitCode string `json:"unit_code" db:"unit_code"`
	UnitName string `json:"unit_name" db:"unit_name"`
	UnitUsage bool `json:"unit_usage" db:"unit_usage"`
	UnitSKU bool `json:"unit_sku" db:"unit_sku"`
	Disabled bool `json:"disabled" db:"disabled"`
}

type UnitDTO struct {
	ID int `json:"unit_id" db:"unit_id"`
	UnitCode string `json:"unit_code" db:"unit_code"`
	UnitName string `json:"unit_name" db:"unit_name"`
	UnitUsage bool `json:"unit_usage" db:"unit_usage"`
	UnitSKU bool `json:"unit_sku" db:"unit_sku"`
	Disabled bool `json:"disabled" db:"disabled"`
}