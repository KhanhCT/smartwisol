package model
import (
	"database/sql"
)

type Shift struct {
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	ShiftName sql.NullString `json:"ShiftName" db:"ShiftName"`
	StartTime int `json:"StartTime" db:"StartTime"`
	StopTime int `json:"StopTime" db:"StopTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type ShiftDTO struct {
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	ShiftName string `json:"ShiftName" db:"ShiftName"`
	StartTime int `json:"StartTime" db:"StartTime"`
	StopTime int `json:"StopTime" db:"StopTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

func (shift Shift) Model2DTO() ShiftDTO{
	dto := ShiftDTO{
		FactoryID: shift.FactoryID,
		ShiftID: shift.ShiftID,
		ShiftName: shift.ShiftName.String,
		StartTime: shift.StartTime,
		StopTime: shift.StopTime,
		Disabled: shift.Disabled,
	}
	return dto
}
