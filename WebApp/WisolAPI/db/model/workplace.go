package model

type WorkPlace struct {
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	WorkPlaceID int `json:"WorkPlaceID" db:"WorkPlaceID"`
	ProductID int `json:"ProductID" db:"ProductID"`
	Code string `json:"WorkPlaceCode" db:"WorkPlaceCode"`
	CycleTime float64 `json:"CycleTime" db:"CycleTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type WorkPlaceDTO struct {
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	WorkPlaceID int `json:"WorkPlaceID" db:"WorkPlaceID"`
	ProductID int `json:"ProductID" db:"ProductID"`
	Code string `json:"WorkPlaceCode" db:"WorkPlaceCode"`
	CycleTime float64 `json:"CycleTime" db:"CycleTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

