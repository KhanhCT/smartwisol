package model

import (
	"database/sql"
)

type Sysvar struct {
	VarName string `json:"VarName"  db:"VarName"`
	VarType sql.NullString `json:"VarType"  db:"VarType"`
	Description sql.NullString `json:"Description"  db:"Description"`
	VarVal string `json:"VarVal"  db:"VarVal"`
	InputMask sql.NullString `json:"InputMask"  db:"InputMask"`
	Disabled bool `json:"Disabled"  db:"disabled"`
}