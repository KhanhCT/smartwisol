package model
import (
	"database/sql"
)

type Scheduler struct {
	ID int `json:"ID" db:"ID"`
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	Description sql.NullString `json:"Description" db:"Description"`
	StartTime int `json:"StartTime" db:"StartTime"`
	StopTime int `json:"StopTime" db:"StopTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}

type SchedulerDTO struct {
	ID int `json:"ID" db:"ID"`
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	Description string `json:"Description" db:"Description"`
	StartTime int `json:"StartTime" db:"StartTime"`
	StopTime int `json:"StopTime" db:"StopTime"`
	Disabled bool `json:"Disabled" db:"Disabled"`
}
func (s Scheduler) Model2DTO() SchedulerDTO {
	dto := SchedulerDTO {
		ID: s.ID,
		FactoryID: s.FactoryID,
		LineID: s.LineID,
		ShiftID: s.ShiftID,
		Description: s.Description.String,
		StartTime: s.StartTime,
		StopTime: s.StopTime,
		Disabled: s.Disabled,
	}
	return dto
}
func (s SchedulerDTO) DTO2Model() Scheduler {
	dto := Scheduler {
		ID: s.ID,
		FactoryID: s.FactoryID,
		LineID: s.LineID,
		ShiftID: s.ShiftID,
		Description: sql.NullString{
			String: s.Description,
			Valid : s.Description != "",
		},
		StartTime: s.StartTime,
		StopTime: s.StopTime,
		Disabled: s.Disabled,
	}
	return dto
}