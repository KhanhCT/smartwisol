package model
import (
	"database/sql"
	"time"

	"wisol/utils"
)

type ProductionDtl struct {
	WorkingDate time.Time `json:"WorkingDate" db:"WorkingDate"`
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	WorkPlaceID int `json:"WorkPlaceID" db:"WorkPlaceID"`
	ProductID int `json:"ProductID" db:"ProductID"`
	StatusCode string `json:"StatusCode" db:"StatusCode"`
	StatusCounter int `json:"StatusCounter" db:"StatusCounter"`
	StatusMsg string `json:"StatusMsg" db:"StatusMsg"`
	WorkerID sql.NullInt64 `json:"WorkerID" db:"WorkerID"`
	StartTime time.Time  `json:"StartTime" db:"StartTime"`
	StopTime time.Time `json:"StopTime" db:"StopTime"`
	Finished bool `json:"Finished" db:"Finished"`
	WorkingTime int
}

type ProductionDtlDTO struct {
	WorkingDate string `json:"WorkingDate" db:"WorkingDate"`
	FactoryID int `json:"FactoryID" db:"FactoryID"`
	LineID int `json:"LineID" db:"LineID"`
	ShiftID int `json:"ShiftID" db:"ShiftID"`
	WorkPlaceID int `json:"WorkPlaceID" db:"WorkPlaceID"`
	ProductID int `json:"ProductID" db:"ProductID"`
	StatusCode string `json:"StatusCode" db:"StatusCode"`
	StatusCounter int `json:"StatusCounter" db:"StatusCounter"`
	StatusMsg string `json:"StatusMsg" db:"StatusMsg"`
	WorkerID int `json:"WorkerID" db:"WorkerID"`
	StartTime string  `json:"StartTime" db:"StartTime"`
	StopTime string `json:"StopTime" db:"StopTime"`
	Finished bool `json:"Finished" db:"Finished"`
} 
func (dto ProductionDtlDTO) DTO2Model()(ProductionDtl, error){
	workingDate, err := time.Parse(utils.YYYY_MM_DD, dto.WorkingDate)
	if err != nil {
		return ProductionDtl{}, err
	}
	
	startTime, err := time.Parse(utils.YYYY_MM_DD_HH_MM_SS, dto.StartTime)
	if err != nil {
		return ProductionDtl{}, err
	}
	stopTime, err := time.Parse(utils.YYYY_MM_DD_HH_MM_SS, dto.StopTime)
	if err != nil {
		return ProductionDtl{}, err
	}
	model := ProductionDtl{
		WorkingDate: workingDate,
		FactoryID: dto.FactoryID,
		LineID: dto.LineID,
		ShiftID: dto.ShiftID,
		WorkPlaceID: dto.WorkPlaceID,
		ProductID: dto.ProductID,
		StatusCode: dto.StatusCode,	
		StatusCounter: dto.StatusCounter,
		StatusMsg: dto.StatusMsg,
		WorkerID: sql.NullInt64{
			Int64: int64(dto.WorkerID),
			Valid: dto.WorkerID != 0,
		},
		StartTime: startTime,
		StopTime: stopTime,
		Finished: dto.Finished,
	}
	return model, nil
}


