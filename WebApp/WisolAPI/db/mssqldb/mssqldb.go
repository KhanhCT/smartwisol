package mssqldb

import (
	"database/sql"
	"fmt"
	"flag"
	"sync"

	"github.com/spf13/viper"
	_ "github.com/denisenkom/go-mssqldb"
	
)

type MssqlDB struct{
	db *sql.DB
	mux sync.Mutex
}

var instance *MssqlDB

func GetInstance() (*MssqlDB){
	if instance == nil {
		Init()
	}
	return instance
}

func Close(){
	if instance != nil{
		instance.db.Close()
	}
}
func (ms *MssqlDB) Lock(){
	ms.mux.Lock()
}
func (ms *MssqlDB) Unlock(){
	ms.mux.Unlock()
}
func (ms *MssqlDB) Tx() (*sql.Tx, error) {
	ms.Lock()
	tx, err := ms.db.Begin()
	defer ms.Unlock()
	return tx, err
}

func Init() error {
	var err error
	// get database information
	hostname := viper.GetString("mssql.address")
	user 	:= viper.GetString("mssql.username")
	pass 	:= viper.GetString("mssql.password")
	port 	:= viper.GetString("mssql.port")
	database := viper.GetString("mssql.database")
	server   := flag.String("S", hostname, "server_name[\\instance_name]")
	connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%s;database=%s",*server , user, pass, port, database)
	instance = new(MssqlDB)
	instance.db, err = sql.Open("mssql", connString)
	if err != nil {
		return err
	}
	err  = instance.db.Ping()
	if err != nil {
		return err
	}
	return nil
}