package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"
)

type ShiftService  struct {
	*CRUD
}

// func (cs *ShiftService) New(obj model.Shift) error{
// 	return cs.Insert("Scheduler VALUES(?, ?, ?, ?, ?, ?, ?, ?)", obj.FactoryID, obj.LineID, obj.ShiftID, obj.TagCode, obj.Description, obj.StartTime, obj.StopTime, obj.Disabled)
// }

func (cs *ShiftService) UpdateScheduler(obj model.Shift) error {
	return nil
}
func (cs *ShiftService) GetAll(query string, args ...interface{}) ([]model.ShiftDTO, error){
	var result []model.ShiftDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ShiftDTO{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.ShiftDTO{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.Shift 
		err:= rows.Scan(&obj.FactoryID, &obj.ShiftID, &obj.ShiftName,  &obj.StartTime, &obj.StopTime, &obj.Disabled)
		if (err == nil){
			result = append(result, obj.Model2DTO())
		}
	}
	tx.Commit()
	return result, nil
}
