package service

import (
	"strings"
	"wisol/db/model"
	"wisol/db/mssqldb"
)

type RoleService struct{CRUD}

func (ds *RoleService) InsertRole(role model.Role) error{
	return ds.Insert("Role VALUES(?,?,?,?)", role.Name, role.CreatedDate, role.Description, role.Disabled)
}

func (ds *RoleService) UpdateRole(role model.Role) error {
	return ds.Update("Role SET(?, ?, ?, ?) WHERE ID=?", &role.Name, &role.CreatedDate, &role.Description, &role.Disabled, &role.ID)
}

func (ds *RoleService) DeleteRoles(ids []interface{}) error {
	return ds.Delete("FROM Role WHERE ID in (?" + strings.Repeat(",?", len(ids)-1) + ")", ids...)
}


func (ds *RoleService) GetRoles(query  string , args ...interface{}) []model.Role{
	var result []model.Role
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.Role{}
	}
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.Role{}
	}
	defer rows.Close()
	for rows.Next(){
		var role model.Role 
		rows.Scan(&role.ID, &role.Name,  &role.CreatedDate, &role.Description, &role.Disabled)
		result = append(result, role)
	}
	tx.Commit()
	return result
}