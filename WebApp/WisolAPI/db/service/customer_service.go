package service
import (
	//"log"
	"wisol/db/model"
	"encoding/json"
	"fmt"
)

type CustomerService  struct {
	*CRUD
}

func (cs *CustomerService) New(cus model.Customer) error{
	return cs.Insert("Customer VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",cus.ID ,cus.FirstName, cus.LastName, cus.Gender, cus.JobTitle, cus.Address, cus.PhoneNum, cus.Age, cus.Nationality, cus.CreatedDate, cus.DeptID, cus.CardID,  cus.UserID, cus.TypeID, cus.PPID, cus.ParentID, cus.Balance, cus.DiscPrice, cus.Contact, cus.Email, cus.Remark, cus.Status)
}
func (cs *CustomerService) NewCustomers(dtos []model.CustomerDTO) error{
	cusJson, err := json.Marshal(dtos)
	if err != nil {
		return err
	}
	fmt.Println(string(cusJson))
	return cs.Exec("sp_InsertCustomers ?", string(cusJson))
}

func (cs *CustomerService) UpdateCustomer(cus model.Customer) error {
	return cs.Update("Customer SET Code=?, FirstName=?, LastName=?, Gender=?, JobTitle=?, Address=?, PhoneNum=?, Age=?, Nationality=?, CreatedDate=?, DeptID=?, CardID=?, UserID=?, TypeID=?, PPID=?, ParentID=?, Balance=?, DiscPrice=?, Contact=?, Email=?, Remark=?, Status=?) WHERE ID=?",&cus.Code, &cus.FirstName, &cus.LastName, &cus.Gender, &cus.JobTitle, &cus.Address, &cus.PhoneNum, &cus.Age, &cus.Nationality, &cus.CreatedDate, &cus.DeptID, &cus.CardID, &cus.UserID, &cus.TypeID, &cus.PPID, &cus.ParentID, &cus.Balance, cus.DiscPrice, &cus.Contact, &cus.Email, &cus.Remark, &cus.Status, &cus.ID)
}

func (cs *CustomerService) GetCustomers(query  string , args ...interface{}) []model.Customer {
	var result []model.Customer
	rows := cs.Get(query, args...)
	if rows == nil {
		return []model.Customer{}
	}
	
	for rows.Next(){
		var cus model.Customer 
		rows.Scan(&cus.ID, &cus.ID , &cus.FirstName, &cus.LastName, &cus.Gender, &cus.JobTitle, &cus.Address, &cus.PhoneNum, &cus.Age, &cus.Nationality, &cus.CreatedDate, &cus.DeptID, &cus.CardID, &cus.UserID, &cus.TypeID, &cus.PPID, &cus.ParentID, &cus.Balance, cus.DiscPrice, &cus.Contact, &cus.Email, &cus.Remark, &cus.Status)
		result = append(result, cus)
	}
	return result
}

func (cs *CustomerService) DisableCustomer(id string) error {
	return cs.Update("Customer SET(?) WHERE ID=?", false,  id)
}


