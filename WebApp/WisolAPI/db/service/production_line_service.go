package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"

)

type ProductionLineService  struct {
	*CRUD
}

func (pps *ProductionLineService) GetAll(query string, args ...interface{}) ([]model.ProductionLineDTO, error){
	var result []model.ProductionLineDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ProductionLineDTO{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.ProductionLineDTO{}, err
	}
	defer rows.Close()
	for rows.Next(){
		var obj model.ProductionLine 
		err := rows.Scan(&obj.FactoryID, &obj.LineID, &obj.LineCode, &obj.Description, &obj.WorkPlaceQty, &obj.Disabled)
			if (err == nil){
			result = append(result, obj.Model2DTO())
		}
	}
	tx.Commit()
	return result, nil
}
