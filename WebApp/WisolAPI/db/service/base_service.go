package service

import (
	"database/sql"
	"wisol/db/mssqldb"
)

type CRUD struct {

}
func (as *CRUD) Insert(query string, args ...interface{}) error{
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		tx.Rollback()
		return err
	}
	
	_, err = tx.Exec("INSERT INTO " + query, args...)
	if err != nil{
		tx.Rollback()
		return err
	} 
	err = tx.Commit()
	return err
}

func (as *CRUD) Get(sqlQuery string, args ...interface{}) *sql.Rows {
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return nil
	}
	rows, err := tx.Query(sqlQuery, args...)
	if err != nil {
		tx.Rollback()
		return nil
	}
	//defer rows.Close()
	err = tx.Commit()
	if err != nil{
		return nil
	}
	
	return rows
}
func (as *CRUD) Delete(sqlQuery string , args ...interface{}) error{
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		tx.Rollback()
		return err
	}
	_, err = tx.Exec("DELETE " + sqlQuery, args...)
	if err != nil{
		tx.Rollback()
		return err
	} 
	err = tx.Commit()
	return err
}
func (as *CRUD) Update(sqlQuery string , args ...interface{}) error{
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		tx.Rollback()
		return err
	}
	_, err = tx.Exec("UPDATE " + sqlQuery, args...)
	if err != nil{
		tx.Rollback()
		return err
	} 
	err = tx.Commit()
	return err
}

func (as *CRUD) Exec(query string, args ...interface{}) error{
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		tx.Rollback()
		return err
	}
	
	_, err = tx.Exec(query, args...)
	if err != nil{
		tx.Rollback()
		return err
	} 
	err = tx.Commit()
	return err
}
