package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"

	"fmt"
)

type SchedulerService  struct {
	*CRUD
}

func (cs *SchedulerService) New(obj model.Scheduler) error{
	return cs.Insert("Scheduler VALUES(?, ?, ?, ?, ?, ?, ?)", obj.FactoryID, obj.LineID, obj.ShiftID, obj.Description, obj.StartTime, obj.StopTime, obj.Disabled)
}

func (cs *SchedulerService) UpdateScheduler(obj model.Scheduler) error {
	return nil
}
func (cs *SchedulerService) GetAll(query string, args ...interface{}) ([]model.SchedulerDTO, error){
	var result []model.SchedulerDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.SchedulerDTO{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.SchedulerDTO{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.Scheduler 
		err:= rows.Scan(&obj.ID, &obj.FactoryID, &obj.LineID, &obj.ShiftID, &obj.Description, &obj.StartTime, &obj.StopTime, &obj.Disabled)
		if (err == nil){
			result = append(result, obj.Model2DTO())
		}else{
			fmt.Println(err.Error())
		}
	}
	tx.Commit()
	return result, nil
}
