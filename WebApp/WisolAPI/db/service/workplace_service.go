package service

import (
	"wisol/db/model"
	"wisol/db/mssqldb"

	"fmt"
)

type WorkPlaceService struct{CRUD}
func (pps *WorkPlaceService) New(obj model.WorkPlace) error {
	return pps.Insert("WorkPlace VALUES(?, ?, ?, ?, ?, ?)", obj.WorkPlaceID, obj.FactoryID, obj.LineID, obj.Code, obj.CycleTime, obj.Disabled)
}
func (ds *WorkPlaceService) UpdateCycleTime(obj model.WorkPlace) error {
	return ds.Update("WorkPlace SET CycleTime=? WHERE WorkPlaceID=? AND FactoryID=? AND LineID=?", &obj.CycleTime, &obj.WorkPlaceID, &obj.FactoryID, &obj.LineID)
}

func (cs *WorkPlaceService) GetAll(query string, args ...interface{}) ([]model.WorkPlace, error){
	var result []model.WorkPlace
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.WorkPlace{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.WorkPlace{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.WorkPlace 
		err := rows.Scan(&obj.WorkPlaceID, &obj.FactoryID, &obj.LineID, &obj.Code, &obj.CycleTime, &obj.Disabled)
		if (err == nil){
			result = append(result, obj)
		}else{
			fmt.Println(err.Error())
		}
	}
	tx.Commit()
	return result, nil
}

func (ws *WorkPlaceService) GetWorkPlace(query string, args ...interface{}) (model.WorkPlace, error) {
	var result model.WorkPlace
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return model.WorkPlace{}, err
	}
	rows, err := tx.Query(query, args...)
	if err != nil {
		fmt.Println(err.Error())
		tx.Rollback()
		return model.WorkPlace{}, err
	}
	defer rows.Close()
	for rows.Next(){
		var obj model.WorkPlace
		err := rows.Scan(&obj.WorkPlaceID, &obj.FactoryID, &obj.LineID, &obj.ProductID, &obj.Code, &obj.CycleTime, &obj.Disabled)
		if (err == nil){
			result = obj
			break			
		}	
	}
	tx.Commit()
	return result, nil
}
