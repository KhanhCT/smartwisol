package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"

	"fmt"
)
type ProductService  struct {
	*CRUD
}
func (pps *ProductService) GetAll(query string, args ...interface{}) []model.ProductDTO{
	var result []model.ProductDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ProductDTO{}
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.ProductDTO{}
	}
	defer rows.Close()
	for rows.Next(){
		var obj model.Product 
		err := rows.Scan( &obj.ProductID, &obj.Barcode, &obj.ProductName, &obj.UnitID, &obj.Disabled)
		dto := obj.Model2DTO()
		if (err == nil){
			result = append(result, dto)
		}else{
			fmt.Println(err.Error())
		}
	}
	tx.Commit()
	return result
}