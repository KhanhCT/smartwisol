package service

import (
	"wisol/db/model"
)

type SysvarService struct{CRUD}

func (ds *SysvarService) IndertSysvar(sysvar model.Sysvar) error{
	return ds.Insert("Sysvar VALUES(?,?,?,?,?,?)", sysvar.VarName, sysvar.VarType, sysvar.Description, sysvar.VarVal, sysvar.InputMask ,sysvar.Disabled)
}

func (ds *SysvarService) UpdateSysvar(Sysvar model.Sysvar) error {
	return ds.Update("Sysvar SET(?,?,?,?,?) WHERE VarName=?",  &Sysvar.VarType, &Sysvar.Description,&Sysvar.VarVal,&Sysvar.InputMask, &Sysvar.Disabled, &Sysvar.VarName)
}

func (ds *SysvarService) GetSysvars(query  string , args ...interface{}) []model.Sysvar{
	var result []model.Sysvar
	rows := ds.Get(query, args...)
	if rows == nil {
		return []model.Sysvar{}
	}
	
	for rows.Next(){
		var Sysvar model.Sysvar 
		rows.Scan(&Sysvar.VarName, &Sysvar.VarType, &Sysvar.Description,&Sysvar.VarVal,&Sysvar.InputMask, &Sysvar.Disabled)
		result = append(result, Sysvar)
	}
	return result
}