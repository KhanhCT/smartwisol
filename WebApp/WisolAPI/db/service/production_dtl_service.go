package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"
)

type ProductionDtlService  struct {
	*CRUD
}

func (ps *ProductionDtlService) New(obj model.ProductionDtl) error {
	return ps.Insert("ProductionDtl VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", obj.WorkingDate, obj.FactoryID, obj.ShiftID, obj.LineID,  obj.WorkPlaceID, obj.ProductID, obj.StatusCode, obj.StatusCounter, obj.StatusMsg, obj.WorkerID, obj.StartTime, obj.StopTime, obj.Finished)
}

func (ps *ProductionDtlService) PutDoneJob(obj model.ProductionDtl) error {
	return ps.Update("ProductionDtl SET StopTime=?, Finished=? WHERE WorkingDate=? AND FactoryID=? AND LineID=? AND ShiftID=? AND WorkPlaceID=? AND StatusCode=? AND StatusCounter=?", obj.StopTime, true, obj.WorkingDate, obj.FactoryID, obj.LineID, obj.ShiftID, obj.WorkPlaceID, obj.StatusCode, obj.StatusCounter)
}
func (ps *ProductionDtlService) UpdateStopTime(obj model.ProductionDtl) error {
	return ps.Update("ProductionDtl SET StopTime=? WHERE WorkingDate=? AND FactoryID=? AND LineID=? AND ShiftID=? AND WorkPlaceID=? AND StatusCode=? AND StatusCounter=?", obj.StopTime, obj.WorkingDate, obj.FactoryID, obj.LineID, obj.ShiftID, obj.WorkPlaceID, obj.StatusCode, obj.StatusCounter)
}

func (pps *ProductionDtlService) GetAll(query string, args ...interface{}) ([]model.ProductionDtl, error){
	var result []model.ProductionDtl
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ProductionDtl{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.ProductionDtl{}, err
	}
	defer rows.Close()
	for rows.Next(){
		var obj model.ProductionDtl 
		err := rows.Scan( &obj.WorkingDate, &obj.FactoryID, &obj.LineID, &obj.ShiftID, &obj.WorkPlaceID, &obj.ProductID, &obj.StatusCode, &obj.StatusCounter, &obj.StatusMsg, &obj.WorkerID,&obj.StartTime, &obj.StopTime, &obj.Finished)
		if (err == nil){
			result = append(result, obj)
		}
	}
	tx.Commit()
	return result, nil
}
func (pps *ProductionDtlService) GetMaxStatusCounter(query string , args ...interface{}) (int, error){
	var result int
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return 0, err
	}
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return 0, err
	}
	defer rows.Close()
	for rows.Next(){
		err := rows.Scan(&result)
		if (err != nil){
			result = 0
		}
	}
	tx.Commit()
	return result, nil
}
func (pps *ProductionDtlService) GetProdDtlByStatus(query string, args ...interface{}) ([]model.ProductionDtl, error){
	var result []model.ProductionDtl
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ProductionDtl{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.ProductionDtl{}, err
	}
	defer rows.Close()
	for rows.Next(){
		var obj model.ProductionDtl 
		err := rows.Scan(&obj.WorkPlaceID, &obj.StatusCode, &obj.WorkingTime)
		if (err == nil){
			result = append(result, obj)
		}
	}
	tx.Commit()
	return result, nil
}