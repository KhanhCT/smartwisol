package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"
	"wisol/crypto"
)

type AccountService  struct {
	*CRUD
}

func (as *AccountService) CheckAccount(username string, password string) bool{
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return false
	}
	
	var ac model.Account
	err = tx.QueryRow("SELECT * FROM Account WHERE username=?", username).Scan(&ac.ID, &ac.Username, &ac.Password, &ac.RoleID, &ac.WorkerID, &ac.Fullname, &ac.CreatedDate, &ac.UpdatedDate, &ac.GroupID, &ac.LastLogin, &ac.Disabled)
	if err != nil {
		tx.Rollback()
		return false
	}

	err = tx.Commit()
	if err != nil{
		return false
	}
	err = new(crypto.Hash).Compare(ac.Password, password)
	if  err != nil {
		return false
	}
	return true
}

func (as *AccountService) New(ac model.Account) error{

	hashPassword, err  := new(crypto.Hash).Generate(ac.Password)
	if err != nil{
		return err
	}
	ac.Password = hashPassword
	return as.Insert("Account VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", ac.Username, ac.Password, ac.RoleID, ac.WorkerID, ac.Fullname, ac.CreatedDate, ac.UpdatedDate, ac.GroupID, ac.LastLogin, ac.Disabled)
}

func (as *AccountService) UpdateAccount(ac model.Account) error{
	return as.Update("Account SET password=?, role_id=?, worker_id=?, fullname=?, created_date=?, updated_date=?, group_id=?, last_login=?, disabled=? WHERE username=?",  ac.Password, ac.RoleID, ac.WorkerID, ac.Fullname, ac.CreatedDate, ac.UpdatedDate, ac.GroupID, ac.LastLogin, ac.Disabled, ac.Username)
}

func (as *AccountService) DeleteAccounts(sqlQuery string , args ...interface{}) error{
	return as.Delete("Account " + sqlQuery, args...)
}

func (as *AccountService)GetAccounts(sqlQuery string, args ...interface{}) []model.Account {

	var acArr []model.Account
	rows := as.Get("SELECT * FROM Account " + sqlQuery, args...)
	if rows == nil{
		return []model.Account{}
	}
	
	isEnded := rows.Next()
	for isEnded{
		var ac model.Account
		rows.Scan(&ac.ID, &ac.Username, &ac.Password, &ac.RoleID, &ac.WorkerID, &ac.Fullname, &ac.CreatedDate, &ac.UpdatedDate, &ac.GroupID, &ac.LastLogin, &ac.Disabled)
		acArr = append(acArr, ac)
		isEnded = rows.Next()
	}
	return acArr
}


