package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"
)

type DeviceService  struct {
	*CRUD
}

func (pps *DeviceService) GetAll(query string, args ...interface{}) []model.DeviceDTO{
	var result []model.DeviceDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.DeviceDTO{}
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.DeviceDTO{}
	}
	defer rows.Close()
	for rows.Next(){
		var obj model.Device 
		err := rows.Scan(&obj.DeviceID, &obj.DeviceName, &obj.FactoryID, &obj.LineID, &obj.WorkPlaceID, &obj.Disabled)
		if (err == nil){
			result = append(result, obj.Model2DTO())
		}
	}
	tx.Commit()
	return result
}