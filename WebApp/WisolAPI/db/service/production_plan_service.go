package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"

	"fmt"
)

type ProductionPlanService  struct {
	*CRUD
}

func (pps *ProductionPlanService) New(obj model.ProductionPlan) error {
	return pps.Insert("ProductionPlan VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)",obj.WorkingDate, obj.FactoryID, obj.LineID, obj.ShiftID, obj.WorkPlaceID,  obj.StartTime, obj.StopTime, obj.ProductID, obj.OrderedQty, obj.GoodProdQty, obj.NGProdQty, obj.Disabled)
}

func (pps *ProductionPlanService) UpdateProductQty(obj model.ProductionPlan) error {
	return pps.Update("ProductionPlan SET OrderedQty=?, GoodProdQty=?, NGProdQty=?, StopTime=?  WHERE FactoryID=? AND LineID=? AND ShiftID=? AND WorkingDate=? AND WorkPlaceID=?", &obj.OrderedQty, &obj.GoodProdQty, &obj.NGProdQty, &obj.StopTime, &obj.FactoryID, &obj.LineID, &obj.ShiftID, &obj.WorkingDate, &obj.WorkPlaceID)
}

func (pps *ProductionPlanService) GetAll(query string, args ...interface{}) ([]model.ProductionPlan, error){
	var result []model.ProductionPlan
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ProductionPlan{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.ProductionPlan{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.ProductionPlan 
		err:= rows.Scan(&obj.WorkingDate, &obj.FactoryID, &obj.LineID, &obj.ShiftID, &obj.WorkPlaceID, &obj.StartTime, &obj.StopTime, &obj.ProductID, &obj.OrderedQty, &obj.GoodProdQty, &obj.NGProdQty, &obj.Disabled)
		if (err == nil){
			result = append(result, obj)
		}
	}
	tx.Commit()
	return result, nil
}
func (pps *ProductionPlanService) GetAllProdPlan(query string, args ...interface{}) ([]model.ProductionPlanDTO, error){
	var result []model.ProductionPlanDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ProductionPlanDTO{}, err
	}
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.ProductionPlanDTO{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.ProductionPlanDTO
		err:= rows.Scan(&obj.WorkingDate, &obj.FactoryID, &obj.LineID, &obj.ShiftID, &obj.WorkPlaceID, &obj.StartTime, &obj.StopTime, &obj.ProductID, &obj.OrderedQty, &obj.GoodProdQty, &obj.NGProdQty, &obj.Disabled, &obj.FactoryName, &obj.LineCode, &obj.ShiftName, &obj.ProductName)
		if (err == nil){
			result = append(result, obj)
		}else{
			fmt.Println(err.Error())
		}
	}
	tx.Commit()
	return result, nil
}
func (pps *ProductionPlanService) GetProdPlanByWorkPlace(query string, args ...interface{}) (model.ProductionPlanDTO, error){
	var result model.ProductionPlanDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return model.ProductionPlanDTO{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return model.ProductionPlanDTO{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.ProductionPlanDTO 
		err:= rows.Scan(&obj.WorkingDate, &obj.FactoryID, &obj.LineID, &obj.ShiftID, &obj.WorkPlaceID, &obj.StartTime, &obj.StopTime, &obj.ProductID, &obj.OrderedQty, &obj.GoodProdQty, &obj.NGProdQty, &obj.Disabled, &obj.ProductName)
		if (err == nil){
			result = obj
		}
	}
	tx.Commit()
	return result, nil
}

func (pps *ProductionPlanService) GetProdPlanByGoal(query string, args ...interface{}) ([]model.GoalObj, error){
	var result []model.GoalObj
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.GoalObj{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.GoalObj{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.GoalObj 
		err:= rows.Scan(&obj.WorkPlaceName, &obj.ProductName,  &obj.OrderedQty, &obj.GoodProdQty)
		if (err == nil){
			if(obj.OrderedQty == 0){
				obj.Goal = "0"
			}else{
				goal := float32(obj.GoodProdQty)/float32(obj.OrderedQty)
				goalStr := fmt.Sprintf("%.1f", goal * 100)
				obj.Goal = goalStr
			}
			result =  append(result, obj)
		}else{fmt.Println(err.Error())}
	}
	tx.Commit()
	return result, nil
}

func (pps *ProductionPlanService) GetProductionByLine(query string, args ...interface{}) ([]model.ProductionPlan, error){
	var result []model.ProductionPlan
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.ProductionPlan{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		fmt.Println(err.Error())
		tx.Rollback()
		return []model.ProductionPlan{}, err
	}
	defer rows.Close()
	for rows.Next(){
		var obj model.ProductionPlan 
		err:= rows.Scan(&obj.LineID, &obj.ProductID,  &obj.GoodProdQty)
		if (err == nil){
			result = append(result, obj)
		}
	}
	tx.Commit()
	return result, nil
}