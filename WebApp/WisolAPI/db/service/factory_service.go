package service
import (
	"wisol/db/model"
	"wisol/db/mssqldb"
)

type FactoryService  struct {
	*CRUD
}
func (cs *FactoryService) UpdateScheduler(obj model.FactoryDTO) error {
	return nil
}
func (cs *FactoryService) GetAll(query string, args ...interface{}) ([]model.FactoryDTO, error){
	var result []model.FactoryDTO
	tx, err := mssqldb.GetInstance().Tx()
	if err != nil{
		return []model.FactoryDTO{}, err
	}
	
	rows, err := tx.Query(query, args...)
	if err != nil {
		tx.Rollback()
		return []model.FactoryDTO{}, err
	}
	defer rows.Close()
	for rows.Next(){	
		var obj model.Factory 
		err:= rows.Scan(&obj.FactoryID, &obj.FactoryCode, &obj.FactoryName,  &obj.ShiftQty, &obj.Disabled)
		if (err == nil){
			result = append(result, obj.Model2DTO())
		}
	}
	tx.Commit()
	return result, nil
}
