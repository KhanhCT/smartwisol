package main

import (

	"flag"
	"fmt"
	"os"
	"log/syslog"
	"time"

	"github.com/kataras/iris"
	"github.com/spf13/viper"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/recover"
	"github.com/dgrijalva/jwt-go"
	"github.com/iris-contrib/middleware/cors"
	"github.com/sirupsen/logrus"
	slhooks "github.com/sirupsen/logrus/hooks/syslog"
	

	jwtmiddleware "wisol/jwt"
	utils "wisol/utils"
	db "wisol/db/mssqldb"
	handler "wisol/handler"
)
var log = logrus.New()
func init() {
	// Only log the warning severity or above.
	viper.SetConfigName("config")     // no need to include file extension
	viper.AddConfigPath("./")  // set the path of your config file
	err := viper.ReadInConfig()

	if err != nil {
	    panic(err)
	}
	if viper.GetString("environment.mode") == "production" {
		Formatter := new(logrus.JSONFormatter)
		Formatter.TimestampFormat = "02-01-2006 15:04:05"
		Formatter.FieldMap =  logrus.FieldMap{
				// FieldKeyTime:  "@timestamp",
				// FieldKeyLevel: "@level",
				// FieldKeyMsg:   "@message",
				// FieldKeyFunc:  "@caller",
		}
		logrus.SetFormatter(Formatter)
	}else{
		Formatter := new(logrus.TextFormatter)
		Formatter.TimestampFormat = "02-01-2006 15:04:05"
		Formatter.FullTimestamp = true
		Formatter.DisableColors = false
		logrus.SetFormatter(Formatter)
	}
	switch viper.GetString("logging.mode") {
		case "panic":
			log.Level = logrus.PanicLevel
		case "fatal":
			log.Level = logrus.FatalLevel
		case "error":
			log.Level = logrus.ErrorLevel
		case "warn":
			log.Level = logrus.WarnLevel
		case "info":
			log.Level = logrus.InfoLevel
		case "debug":
			log.Level = logrus.DebugLevel
		case "trace":
			log.Level = logrus.TraceLevel
		default: log.Level = logrus.InfoLevel		
	}
	if sl, err := slhooks.NewSyslogHook("udp", "localhost:5044", syslog.LOG_INFO, ""); err == nil {
		log.Hooks.Add(sl)
	}else{

	}

	file, err := os.OpenFile("smartmeal.log", os.O_CREATE|os.O_WRONLY, 0666)
	if err == nil {
		log.Out = file
	} else {
		log.Error(err.Error())
	}
	log.Info("Logger Mode:" + log.GetLevel().String() )
}

func getApiPath(name string) string{
	return utils.BASE_URL + name
}

func main() {
	log.Info("Initializing.....")
	log.Info(time.Now())
	flag.Parse()
	port := viper.GetString("server.port")
	ip := viper.GetString("server.ip")
	var serverAddr = fmt.Sprintf("%v:%v", ip, port)

	
	// create restful server
	app := iris.New() 
	app.Logger().SetLevel("debug")
	f, err := os.OpenFile("iris.log", os.O_CREATE|os.O_WRONLY, 0666)
	if err == nil {
		app.Logger().SetOutput(f)
	} else {
		log.Error(err.Error())
	}
	app.Use(recover.New())
	app.Use(logger.New())
	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, // allows everything, use that to change the hosts.
		AllowCredentials: true,
		AllowedMethods: []string{"GET", "POST", "PUT", "HEAD"},
		AllowedHeaders: []string{"Content-Type", "Authorization"},
	})

	app.Use(crs)
	app.AllowMethods(iris.MethodOptions)

	
	//Init database
	err = db.Init()
	if err !=nil {
		log.Panic(err.Error())
		panic(err)
	}else{
		log.Info("Created database connection")
	}

	// free database connection
	defer db.Close()
	
	jwtHandler := jwtmiddleware.New(jwtmiddleware.Config{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				
				return nil, fmt.Errorf("There was an error")
			}
			return []byte("secret"), nil
		},
		ContextKey : utils.AUTHORIZATION,
		Debug: true,
		SigningMethod: jwt.SigningMethodHS256,
	})

	app.Use(jwtHandler.Serve)

	authService := handler.AuthService{Logger: log}
	roleHandler := handler.RoleHandler{Logger: log}
	productionLineHandler := handler.ProductionLineHandler{Logger: log}
	productHandler := handler.ProductHandler{Logger: log}

	//auth api
	app.Post(getApiPath("login"), authService.Login)
	app.Get(getApiPath("logout"), authService.Logout)
	
	// account service 
	app.Post(getApiPath("account"), authService.NewAccount)

	// role
	app.Get(getApiPath("role"), roleHandler.GetAllRoles)
	app.Post(getApiPath("role"), roleHandler.NewRole)

	// production plan
	app.Get(getApiPath("productionplan/line/{FactoryID}/{ShiftID}/{FromDate}/{ToDate}"), productionLineHandler.GetProductionPlanByLine)
	app.Get(getApiPath("productionplan/workplace/{WorkingDate}/{FactoryCode}/{LineCode}/{ShiftID}/{WorkPlaceID}"), productionLineHandler.GetProdPlanByWorkPlace)
	app.Post(getApiPath("productionplan"), productionLineHandler.NewProductionPlan)
	app.Get(getApiPath("productionplan"), productionLineHandler.GetAllProdPlan)
	app.Get(getApiPath("productionplan/goal/{WorkingDate}/{FactoryID}/{LineID}/{ShiftID}"), productionLineHandler.GetProdPlanByGoal)
	app.Get(getApiPath("productionplan/status/{FactoryID}/{LineID}/{ShiftID}"), productionLineHandler.GetScheduler)

	app.Put(getApiPath("productionplan/model"), productionLineHandler.ChangeModel)
	app.Put(getApiPath("productionplan/productqty"), productionLineHandler.UpdateProductQty)

	// production dtl
	app.Get(getApiPath("productiondtl/workplace/{WorkingDate}/{FactoryID}/{LineID}/{ShiftID}/{WorkPlaceID}"), productionLineHandler.GetCurrentStatus)
	app.Get(getApiPath("productiondtl/line/{FactoryID}/{ShiftID}/{LineID}/{FromDate}/{ToDate}"), productionLineHandler.GetProductionDtlByStatus)
	app.Get(getApiPath("productiondtl/maxstatuscounter/{WorkingDate}/{FactoryID}/{LineID}/{ShiftID}/{WorkPlaceID}/{StatusCode}"), productionLineHandler.GetMaxStatusCounter)
	app.Post(getApiPath("productiondtl"), productionLineHandler.NewProductionDtl)
	app.Put(getApiPath("productiondtl"), productionLineHandler.PutDoneJob)
	app.Put(getApiPath("productiondtl/stoptime"), productionLineHandler.UpdateStopTime)

	// production line 
	app.Get(getApiPath("productionline/{FactoryID}"), productionLineHandler.GetProdLine)
	app.Get(getApiPath("productionline/status"), productionLineHandler.GetStatusInfo)

	// Workplace
	app.Post(getApiPath("workplace"), productionLineHandler.NewWorkPlace)
	app.Get(getApiPath("workplace/info/{FactoryID}/{LineID}/{WorkPlaceID}/{ProductID}"), productionLineHandler.GetWorkPlaceInfo)
	app.Put(getApiPath("workplace/cycletime"), productionLineHandler.UpdateCycleTime)

	//product
	app.Get(getApiPath("product"), productHandler.GetAll)

	//shift
	app.Get(getApiPath("shift/{FactoryCode}"), productionLineHandler.GetShiftByFactory)
	app.Get(getApiPath("factory"), productionLineHandler.GetFactory)

	//shift
	app.Get(getApiPath("scheduler/{FactoryID}/{LineID}/{ShiftID}"), productionLineHandler.GetScheduler)
	app.Post(getApiPath("scheduler"), productionLineHandler.NewScheduler)
	// run server
	app.Run(iris.Addr(serverAddr), iris.WithoutServerError(iris.ErrServerClosed))
}


