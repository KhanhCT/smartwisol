package handler
import (

)
type Response struct {
	Status string `json:"Status"`
	Message string `json:"Message"`
	Body interface{} `json:"Data"`
}
type ProdResponse struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Body map[int]interface{} `json:"body"`
}

type ProdDtlResponse struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Body map[string]interface{} `json:"body"`
}