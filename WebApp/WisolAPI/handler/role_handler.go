package handler

import (
	//"time"
	"wisol/db/model"
	"wisol/db/service"
	"github.com/kataras/iris"
	logrus "github.com/sirupsen/logrus"
)

var roleService *service.RoleService =  new(service.RoleService)
type RoleHandler struct{
	Logger *logrus.Logger
}

func (au *RoleHandler)GetAllRoles(ctx iris.Context){
	query := "SELECT * FROM Role"
	roles := roleService.GetRoles(query)
	roleDtos := make([]model.RoleDTO, len(roles))
	for i, role := range(roles){
		roleDtos[i] = role.Model2DTO()
	}
	ctx.JSON(roleDtos)
}

func (au *RoleHandler) NewRole(ctx iris.Context){
	var roleDTO model.RoleDTO
	if err := ctx.ReadJSON(&roleDTO); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	role := roleDTO.DTO2Model()
	role.Disabled = true
	err := roleService.InsertRole(role)
	if err == nil {
		ctx.StatusCode(iris.StatusOK)
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
	}
}

func (au *RoleHandler) UpdateRole(ctx iris.Context){
	var roleDTO model.RoleDTO
	if err := ctx.ReadJSON(&roleDTO); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
}