package handler

import (
	"time"

	"wisol/utils"
	"wisol/db/model"
	"wisol/db/service"

	"github.com/kataras/iris"
	logrus "github.com/sirupsen/logrus"
)


var cusService *service.CustomerService =  new(service.CustomerService)
type CustomerHandler struct{
	Logger *logrus.Logger
}

func (au *CustomerHandler)GetCustomers(ctx iris.Context){
	query := "SELECT * FROM Customer"
	cuses := cusService.GetCustomers(query)
	dtos := make([]model.CustomerDTO, len(cuses))
	for i, m := range(cuses){
		dtos[i] = m.Model2DTO()
	}
	ctx.JSON(dtos)
}

func (au *CustomerHandler) NewCustomers(ctx iris.Context){
	var dtos []model.CustomerDTO
	if err := ctx.ReadJSON(&dtos); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	_time := time.Now().Format(utils.YYYY_MM_DD_HH_MM_SS)
	for i,_ := range(dtos){
		dtos[i].CreatedDate = _time
		dtos[i].Status = true
	}	
	err := cusService.NewCustomers(dtos);
	if err == nil {
		ctx.StatusCode(iris.StatusOK)
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
	}
}

func (au *CustomerHandler) UpdateCustomer(ctx iris.Context){
	var dto model.CustomerDTO
	if err := ctx.ReadJSON(&dto); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	m := dto.DTO2Model()
	err := cusService.UpdateCustomer(m);
	if err == nil {
		ctx.StatusCode(iris.StatusOK)
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
	}
}

func (au *CustomerHandler) DeleteCustomer(ctx iris.Context){
	var dto model.CustomerDTO
	if err := ctx.ReadJSON(&dto); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	m := dto.DTO2Model()
	err := cusService.UpdateCustomer(m);
	if err == nil {
		ctx.StatusCode(iris.StatusOK)
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
	}
}


func (au *CustomerHandler) DisableCustomer(ctx iris.Context){
	id := ctx.Params().Get("id")
	err := cusService.DisableCustomer(id)
	if err == nil {
		ctx.StatusCode(iris.StatusOK)
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
	}
}
