package handler

import(
	"wisol/db/service"
	
	"github.com/kataras/iris"
	logrus "github.com/sirupsen/logrus"
)
type ProductHandler struct {
	Logger *logrus.Logger
}
var productService = new(service.ProductService)

func (ph *ProductHandler) GetAll(ctx iris.Context){
	query := "SELECT * FROM Product"
	var response Response
	dtos := productService.GetAll(query)
	response.Status = "Success"
	response.Message =  "Get all products"
	response.Body = dtos
	ctx.JSON(response)
}