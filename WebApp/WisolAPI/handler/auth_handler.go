package handler

import (
	"fmt"

	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris"
	"wisol/utils"
	"wisol/db/model"
	"wisol/db/service"
	logrus "github.com/sirupsen/logrus"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
	DeviceID string `json:"DeviceID"`
}

type JwtToken struct {
    Token string `json:"token"`
}

type Exception struct {
    Message string `json:"message"`
}

type CustomeClaims struct {
	Meat User `json:"meat"`
	T time.Time `json:"time"`
	jwt.StandardClaims
}

type AuthService struct{
	Logger *logrus.Logger
}


func (as *AuthService)CreateToken(user User) (JwtToken, error) {
	expireToken := time.Now().Add(time.Hour * 8760).Unix()
	claims := CustomeClaims{
		user,
		time.Now(),
		jwt.StandardClaims{
			Issuer:  "CTK",	
			ExpiresAt: expireToken,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,claims)
	tokenString, err := token.SignedString([]byte("secret"))
    if err != nil {
        return JwtToken{}, err
    }
	return JwtToken{Token: tokenString}, nil
}

func (as *AuthService)GetAccounts(ctx iris.Context) {
	// var accountService *service.AccountService = new(service.AccountService)
	// rows := accountService.GetAccounts("")
}

func parseToken(tokenStr string)(User, error){
	var user User 
	token, _ := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte("secret"), nil
	})
	if _, ok := token.Claims.(*CustomeClaims); ok && token.Valid {
		//log.Info(claims)
    } else {
        return User{}, nil
	}
	return user, nil
}

func  (as *AuthService)NewAccount(ctx iris.Context) {
	var account model.AccountDTO
	if err := ctx.ReadJSON(&account); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	account.CreatedDate = time.Now()
	account.UpdatedDate = time.Now()
	account.Disabled = false
	var accountService *service.AccountService = new(service.AccountService)
	model := account.DTO2Model()
	err := accountService.New(model)
	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}else{
		ctx.StatusCode(iris.StatusOK)
		return
	}
}
func  (as *AuthService)UpdateAccount(ctx iris.Context) {
	var account model.Account
	if err := ctx.ReadJSON(&account); err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	account.UpdatedDate = time.Now()
	var accountService *service.AccountService = new(service.AccountService)
	err := accountService.New(account)
	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}else{
		ctx.StatusCode(iris.StatusOK)
		return
	}
}
func (m *AuthService) Logout(ctx iris.Context) {
	ctx.StatusCode(iris.StatusOK)
}


func (as *AuthService)Login(ctx iris.Context) {
	var user User 
	var response Response
	if err := ctx.ReadJSON(&user); err != nil {
		response.Status = "Error"
		response.Message = err.Error()
		data := ""
		response.Body = data
		ctx.JSON(response)
		ctx.StatusCode(iris.StatusBadRequest)
		return 	
	}
	if(user.DeviceID != ""){		
		query := "SELECT * FROM Device WHERE DeviceID=?"
		var deviceService *service.DeviceService = new(service.DeviceService)
		if(len(deviceService.GetAll(query, user.DeviceID))  <= 0){
			response.Status = "Error"
			response.Message = "Device ID is not registered"
			data := ""
			response.Body = data
			ctx.JSON(response)
			ctx.StatusCode(iris.StatusBadRequest)
			return 
		}else{
			user.Username = user.DeviceID
			user.Password = user.DeviceID
		}	
	}else{
		var accountService *service.AccountService = new(service.AccountService)
		if ret := accountService.CheckAccount(user.Username, user.Password); ret == false {
			response.Status = "Error"
			response.Message = "Username or Password is invalid"
			data := ""
			response.Body = data
			ctx.JSON(response)
			ctx.StatusCode(iris.StatusBadRequest)
			return
		}
	}
	token,err := as.CreateToken(user)
	if err != nil {
		response.Status = "Error"
		response.Message = "Cannot create token"
		data := ""
		response.Body = data
		ctx.JSON(response)
		ctx.StatusCode(iris.StatusBadRequest)
	}
	ctx.Values().Set(utils.AUTHORIZATION, token.Token)
	response.Status = "Success"
	response.Message = "Enjoy Token"
	data := token.Token	
	response.Body = data
	ctx.JSON(response)
	ctx.StatusCode(iris.StatusOK)
}

