package handler
import (
	"wisol/db/model"
	"wisol/db/service"
	"wisol/utils"
	"github.com/kataras/iris"
	logrus "github.com/sirupsen/logrus"

	"time"
	"fmt"
)

var prodPlanService *service.ProductionPlanService =  new(service.ProductionPlanService)
var prodDtlService *service.ProductionDtlService =  new(service.ProductionDtlService)
var workplaceService *service.WorkPlaceService =  new(service.WorkPlaceService)
var prodLineService *service.ProductionLineService =  new(service.ProductionLineService)
var shiftService *service.ShiftService =  new(service.ShiftService)
var factoryService *service.FactoryService =  new(service.FactoryService)
var schedulerService *service.SchedulerService =  new(service.SchedulerService)

type ProductionLineHandler struct {
	Logger *logrus.Logger
}

func (handler *ProductionLineHandler) NewProductionPlan(ctx iris.Context) {
	var response Response
	var dto model.ProductionPlanDTO
	if err := ctx.ReadJSON(&dto); err != nil {
		fmt.Println(err.Error())
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return	
	}
	dto.Disabled = false
	dto.StartTime = dto.StopTime
	prodPlan, err := dto.DTO2Model()
	prodPlan.GoodProdQty = 0
	prodPlan.OrderedQty = 0
	prodPlan.NGProdQty=0
	if err != nil{
		fmt.Println(err.Error())
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return 
	}
	if err := prodPlanService.New(prodPlan); err != nil {
		fmt.Println(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "SQL Exception"
	}else{
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
	}
	ctx.JSON(response)
}
func (handler *ProductionLineHandler) ChangeModel(ctx iris.Context) {
	var dto model.ProductionPlanDTO
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	
	workingDate, err := time.Parse(utils.YYYY_MM_DD, dto.WorkingDate)
	if err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	query := "ProductionPlan SET ProductID=? WHERE WorkingDate=? AND FactoryID=? AND LineID=? AND ShiftID=? AND WorkPlaceID=?"
	if err := prodPlanService.Update(query, dto.ProductID,workingDate,dto.FactoryID, dto.LineID, dto.ShiftID, dto.WorkPlaceID); err != nil {
		fmt.Println(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}else{
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		ctx.JSON(response)
	}
}
func (handler *ProductionLineHandler) UpdateProductQty(ctx iris.Context) {
	var dto model.ProductionPlanDTO
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	
	workingDate, err := time.Parse(utils.YYYY_MM_DD, dto.WorkingDate)
	if err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	prodPlan := model.ProductionPlan{
		WorkingDate: workingDate,
		FactoryID: dto.FactoryID,
		LineID: dto.LineID,
		ShiftID: dto.ShiftID,
		ProductID: dto.ProductID,
		WorkPlaceID: dto.WorkPlaceID,
		OrderedQty: dto.OrderedQty,
		GoodProdQty: dto.GoodProdQty,
		StopTime: time.Now(),
		NGProdQty: dto.NGProdQty,
	}
	if err := prodPlanService.UpdateProductQty(prodPlan); err != nil {
		fmt.Println(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}else{
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		ctx.JSON(response)
	}
}

func (handler *ProductionLineHandler) GetAllProdPlan(ctx iris.Context){
	query := "exec [dbo].[sp_GetProdPlan]"
	var response Response
	if dtos,err := prodPlanService.GetAllProdPlan(query); err != nil{
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
	}else{
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		response.Body = dtos
	}
	ctx.JSON(response)
}
func (handler *ProductionLineHandler) GetProductionPlanByLine(ctx iris.Context){
	var response Response
	fromDate := ctx.Params().Get("FromDate")
	toDate := ctx.Params().Get("ToDate")
	factoryID, _ := ctx.Params().GetInt("FactoryID")
	shiftID := ctx.Params().Get("ShiftID")
	query := "exec [dbo].[sp_GetProdPlanByLine] ?, ?, ?, ?"
	dtos, err := prodPlanService.GetProductionByLine(query, factoryID, shiftID, fromDate, toDate)
	if(err != nil){
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}
	if(len(dtos) <=0){
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		ctx.JSON(response)
		return
	}
	lines, err := prodLineService.GetAll("SELECT * FROM ProductionLine WHERE Disabled=0")
	if(err != nil){
		ctx.StatusCode(iris.StatusBadRequest)
		fmt.Println(err.Error())
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}
	var prodRes []ProdResponse
	var lineID int = dtos[0].LineID
	var lineName string 
	var tmp map[int]interface{} = make(map[int]interface{})
	var i int = 1
	for _, val := range dtos {
		if(lineID != val.LineID){
			for _, line := range lines{
				if(line.LineID == lineID){
					lineName = line.LineCode
					break
				}
			}
			
			if(lineName == ""){
				tmp = make(map[int]interface{})
			}else{
				prod := ProdResponse{
					ID: i,
					Name: lineName,
					Body: tmp,
				}
				prodRes = append(prodRes, prod)
				i += 1
				lineName = ""				
				tmp = make(map[int]interface{})
			}
		}
		lineID = val.LineID
		tmp[val.ProductID] = val.GoodProdQty
	}
	for _, line := range lines{
		if(line.LineID == lineID){
			lineName = line.LineCode
			break
		}
	}
	
	if(lineName == ""){
		tmp = make(map[int]interface{})
	}else{
		prod := ProdResponse{
			ID: i,
			Name: lineName,
			Body: tmp,
		}
		prodRes = append(prodRes, prod)
	}
	ctx.StatusCode(iris.StatusOK)
	response.Status = utils.STATUS_SUCCESS
	response.Message = "Enjoy"
	response.Body = prodRes
	ctx.JSON(response) 
}

func (handler *ProductionLineHandler) GetProductionDtlByStatus(ctx iris.Context){
	var response Response
	fromDate := ctx.Params().Get("FromDate")
	toDate := ctx.Params().Get("ToDate")
	factoryID := ctx.Params().Get("FactoryID")
	shiftID := ctx.Params().Get("ShiftID")
	lineID := ctx.Params().Get("LineID")
	query := "exec [dbo].[sp_GetProdDtlByStatus] ?, ?, ?, ?, ?"
	dtos,err := prodDtlService.GetProdDtlByStatus(query, factoryID, lineID, shiftID , fromDate, toDate)
	if(err != nil){
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}
	if(len(dtos) <=0){
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		ctx.JSON(response)
		return
	}
	workPlaces, err := workplaceService.GetAll("SELECT * FROM WorkPlace WHERE FactoryID=? AND LineID=? AND Disabled=0", factoryID, lineID)
	if(err != nil){
		ctx.StatusCode(iris.StatusBadRequest)
		fmt.Println(err.Error())
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}
	var prodRes []ProdDtlResponse
	var workPlaceID int = workPlaces[0].WorkPlaceID
	var workPlaceName string 
	var tmp map[string]interface{} = make(map[string]interface{})

	var i int = 1
	for _, val := range dtos {
		if(workPlaceID != val.WorkPlaceID){
			for _, wp := range workPlaces{
				if(wp.WorkPlaceID == workPlaceID){
					workPlaceName = wp.Code
					break
				}
			}
			if(workPlaceName == ""){
				tmp = make(map[string]interface{})
			}else{
				prod := ProdDtlResponse{
					ID: i,
					Name: workPlaceName,
					Body: tmp,
				}
				prodRes = append(prodRes, prod)
				i += 1
				workPlaceName = ""				
				tmp = make(map[string]interface{})
			}
		}
		workPlaceID = val.WorkPlaceID
		tmp[val.StatusCode] = val.WorkingTime
	}

	for _, wp := range workPlaces{
		if(wp.WorkPlaceID == workPlaceID){
			workPlaceName = wp.Code
			break
		}
	}
	
	if(workPlaceName == ""){
		tmp = make(map[string]interface{})
	}else{
		prod := ProdDtlResponse{
			ID: i,
			Name: workPlaceName,
			Body: tmp,
		}
		prodRes = append(prodRes, prod)
	}
	ctx.StatusCode(iris.StatusOK)
	response.Status = utils.STATUS_SUCCESS
	response.Message = "Enjoy"
	response.Body = prodRes
	ctx.JSON(response) 
}
func (handler *ProductionLineHandler) GetProdPlanByGoal(ctx iris.Context){
	var response Response
	factoryID := ctx.Params().Get("FactoryID")
	lineID := ctx.Params().Get("LineID")
	shiftID := ctx.Params().Get("ShiftID")
	workingDate := ctx.Params().Get("WorkingDate")

	query := "SELECT t3.Code , t2.ProductName ,  t1.OrderedQty , t1.GoodProdQty  FROM ProductionPlan AS t1 JOIN Product AS t2 ON t1.ProductID=t2.ProductID JOIN WorkPlace AS t3 ON t1.WorkPlaceID=t3.WorkPlaceID WHERE t1.WorkingDate=? AND t1.FactoryID=? AND t1.LineID=? AND t1.ShiftID=?"
	dto,err := prodPlanService.GetProdPlanByGoal(query, workingDate, factoryID, lineID, shiftID)
	if(err!= nil){
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}
	response.Body = dto
	response.Status = utils.STATUS_SUCCESS
	response.Message = "Enjoy"
	ctx.JSON(&response)
}

func (handler *ProductionLineHandler) GetProdPlanByWorkPlace(ctx iris.Context){
	var response Response
	factoryCode := ctx.Params().Get("FactoryCode")
	lineCode := ctx.Params().Get("LineCode")
	shiftID := ctx.Params().Get("ShiftID")
	workPlaceID := ctx.Params().Get("WorkPlaceID")
	workingDate := ctx.Params().Get("WorkingDate")
	query := "exec [dbo].[sp_GetProductionPlanInfo] ?, ?, ?, ?, ?"
	dto,err := prodPlanService.GetProdPlanByWorkPlace(query, workingDate, factoryCode, lineCode, shiftID, workPlaceID)
	if(err!= nil){
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return
	}
	response.Body = dto
	response.Status = utils.STATUS_SUCCESS
	response.Message = "Enjoy"
	ctx.JSON(&response)
}

func (handler *ProductionLineHandler) NewProductionDtl(ctx iris.Context) {
	var dto model.ProductionDtlDTO
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		fmt.Println(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.JSON(response)
		return	
	}
	workingDate, err := time.Parse(utils.YYYY_MM_DD, dto.WorkingDate)
	if err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}

	prodDtl := model.ProductionDtl{
		FactoryID: dto.FactoryID,
		LineID: dto.LineID,
		ShiftID: dto.ShiftID,
		WorkPlaceID: dto.WorkPlaceID,
		ProductID: dto.ProductID,
		StatusCode: dto.StatusCode,	
		StatusCounter: dto.StatusCounter,
		StatusMsg: dto.StatusMsg,
		Finished: dto.Finished,
	}
	now := time.Now()
	prodDtl.WorkingDate = workingDate
	prodDtl.StartTime = now
	prodDtl.StopTime = now
	prodDtl.Finished = false 
	if err := prodDtlService.New(prodDtl); err != nil {
		fmt.Println(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		
	}else{
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Done"
	}
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) GetMaxStatusCounter(ctx iris.Context){
	var response Response	
	factoryID := ctx.Params().Get("FactoryID")
	lineID := ctx.Params().Get("LineID")
	shiftID := ctx.Params().Get("ShiftID")
	workPlaceID := ctx.Params().Get("WorkPlaceID")
	workingDate := ctx.Params().Get("WorkingDate")
	statusCode := ctx.Params().Get("StatusCode")
	query := "SELECT MAX(StatusCounter) AS StatusCounter FROM ProductionDtl WHERE WorkingDate=? AND FactoryID=? AND LineID=? AND ShiftID=? AND WorkPlaceID=? AND StatusCode=?"
	if ret, err := prodDtlService.GetMaxStatusCounter(query, workingDate, factoryID, lineID, shiftID, workPlaceID, statusCode); err!= nil{
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		response.Body = ret
		ctx.StatusCode(iris.StatusBadRequest)
	}else{
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		response.Body = ret
		ctx.StatusCode(iris.StatusOK)
	}
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) PutDoneJob(ctx iris.Context){
	var dto model.ProductionDtlDTO
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	now := time.Now()
	workingDate, err := time.Parse(utils.YYYY_MM_DD, dto.WorkingDate)
	if err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	// Just avoid parsing failed
	prodDtl := model.ProductionDtl{
		FactoryID: dto.FactoryID,
		LineID: dto.LineID,
		ShiftID: dto.ShiftID,
		WorkPlaceID: dto.WorkPlaceID,
		ProductID: dto.ProductID,
		StatusCode: dto.StatusCode,	
		StatusCounter: dto.StatusCounter,
		StatusMsg: dto.StatusMsg,
		Finished: dto.Finished,
	}
	prodDtl.StopTime = now
	prodDtl.WorkingDate = workingDate
	prodDtl.Finished = true
	fmt.Println(workingDate)
	err = prodDtlService.PutDoneJob(prodDtl)
	if err == nil {
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Put done job"
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "SQL Exception"
	}
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) GetCurrentStatus(ctx iris.Context){
	var response Response	
	factoryID := ctx.Params().Get("FactoryID")
	lineID := ctx.Params().Get("LineID")
	shiftID := ctx.Params().Get("ShiftID")
	workPlaceID := ctx.Params().Get("WorkPlaceID")
	workingDate := ctx.Params().Get("WorkingDate")
	query := "SELECT * FROM ProductionDtl WHERE WorkingDate=? AND FactoryID=? AND LineID=? AND ShiftID=? AND WorkPlaceID=? AND Finished=0"	
	dtos, err := prodDtlService.GetAll(query, workingDate, factoryID, lineID, shiftID, workPlaceID)
	if(err != nil){
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		response.Body = nil
		return
	}
	response.Status = utils.STATUS_SUCCESS
	response.Message = "Enjoy"
	if(len(dtos) > 0){
		response.Body = dtos[0]
	}else{
		response.Body = nil
	}
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) GetWorkPlaceInfo(ctx iris.Context){
	var response Response
	query := "SELECT * FROM WorkPlace WHERE FactoryID=? AND LineID=? AND WorkPlaceID=? AND ProductID=?"
	workplaceID, _ := ctx.Params().GetInt("WorkPlaceID")
	factoryID := ctx.Params().GetString("FactoryID")
	lineID := ctx.Params().GetString("LineID")
	productID := ctx.Params().GetString("ProductID")
	dtos, err := workplaceService.GetWorkPlace(query, factoryID, lineID, workplaceID, productID);
	if( err == nil){
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Get workplace"
		response.Body = dtos
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "Error"
	}
	ctx.JSON(response)
}
func (handler *ProductionLineHandler) NewWorkPlace(ctx iris.Context){
	var dto model.WorkPlace
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	if err := workplaceService.New(dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
	}else{
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		ctx.StatusCode(iris.StatusOK)
	}
	ctx.JSON(response)
}
func (handler *ProductionLineHandler) UpdateCycleTime(ctx iris.Context){
	var dto model.WorkPlace
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	if err := workplaceService.UpdateCycleTime(dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
	}else{
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		ctx.StatusCode(iris.StatusOK)
	}
	ctx.JSON(response)
}
func (handler *ProductionLineHandler) UpdateStopTime(ctx iris.Context){
	var dto model.ProductionDtlDTO
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		fmt.Println(err.Error())
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	now := time.Now()
	workingDate, err := time.Parse(utils.YYYY_MM_DD, dto.WorkingDate)
	if err != nil {
		fmt.Println(err.Error())
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	// Just avoid parsing failed
	prodDtl := model.ProductionDtl{
		FactoryID: dto.FactoryID,
		LineID: dto.LineID,
		ShiftID: dto.ShiftID,
		WorkPlaceID: dto.WorkPlaceID,
		ProductID: dto.ProductID,
		StatusCode: dto.StatusCode,	
		StatusCounter: dto.StatusCounter,
		StatusMsg: dto.StatusMsg,
		StopTime: now,
		WorkingDate: workingDate,
	}
	fmt.Println(prodDtl)
	err = prodDtlService.UpdateStopTime(prodDtl)
	if err == nil {
		
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Put done job"
	}else{
		fmt.Println(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "SQL Exception"
	}
	ctx.JSON(response)
}


func (handler *ProductionLineHandler) GetShiftByFactory(ctx iris.Context){
	var response Response
	factoryCode := ctx.Params().Get("FactoryCode")
	query := "SELECT t1.* FROM Shift AS t1 JOIN Factory AS t2 ON t1.FactoryID = t2.FactoryID WHERE t2.FactoryCode=? AND t1.Disabled=0"	
	dtos, err := shiftService.GetAll(query, factoryCode)
	if( err == nil){
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Get Shift"
		response.Body = dtos
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "Error"
	}
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) GetFactory(ctx iris.Context){
	var response Response
	query := "SELECT * FROM Factory WHERE Disabled=0"	
	dtos, err := factoryService.GetAll(query)
	if( err == nil){
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Get Factory"
		response.Body = dtos
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "Error"
	}
	ctx.JSON(response)
}
func (handler *ProductionLineHandler) GetProdLine(ctx iris.Context){
	var response Response	
	factoryID := ctx.Params().Get("FactoryID")
	query := "SELECT * FROM ProductionLine WHERE factoryID=? AND Disabled=0"	
	dtos, err := prodLineService.GetAll(query, factoryID)
	if( err == nil){
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		response.Body = dtos
	}else{
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "Error"
	}
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) GetStatusInfo(ctx iris.Context){
	var statusObj [6]model.Status
	var response Response
	statusObj[0] = model.Status{
		ID : "01",
		Name: "PRODUCTION IN PROGRESS",
		Color: "#00FF00",
	}
	statusObj[1] = model.Status{
		ID : "02",
		Name: "MATERIAL SHORTAGE",
		Color: "#FFCC33",
	}
	statusObj[2] = model.Status{
		ID : "03",
		Name: "NO PRODUCTION",
		Color: "#CCCCCC",
	}
	statusObj[3] = model.Status{
		ID : "04",
		Name: "LINE DOWN",
		Color: "#FF0000",
	}
	statusObj[4] = model.Status{
		ID : "05",
		Name: "CHANGE MODEL",
		Color: "#FF0000",
	}
	statusObj[5] = model.Status{
		ID : "06",
		Name: "MAINTAINANCE IN PROGRESS",
		Color: "#0000FF",
	}
	ctx.StatusCode(iris.StatusOK)
	response.Status = utils.STATUS_SUCCESS
	response.Message = "Enjoy"
	response.Body = statusObj
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) NewScheduler(ctx iris.Context){
	var dto model.SchedulerDTO
	var response Response
	if err := ctx.ReadJSON(&dto); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(response)
		return
	}
	if err := schedulerService.New(dto.DTO2Model()); err != nil {
		response.Status = utils.STATUS_ERROR
		response.Message = err.Error()
		ctx.StatusCode(iris.StatusBadRequest)
	}else{
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		ctx.StatusCode(iris.StatusOK)
	}
	ctx.JSON(response)
}

func (handler *ProductionLineHandler) GetScheduler(ctx iris.Context){
	var response Response	
	factoryID := ctx.Params().Get("FactoryID")
	lineID := ctx.Params().Get("LineID")
	shiftID := ctx.Params().Get("ShiftID")
	fmt.Println(lineID)
	query := "SELECT * FROM Scheduler WHERE FactoryID=? AND LineID=? AND ShiftID=? Order By FactoryID, LineID, ShiftID"	
	dtos, err := schedulerService.GetAll(query, factoryID, lineID, shiftID)
	if( err == nil){
		ctx.StatusCode(iris.StatusOK)
		response.Status = utils.STATUS_SUCCESS
		response.Message = "Enjoy"
		response.Body = dtos
	}else{
		fmt.Println(err.Error())
		ctx.StatusCode(iris.StatusBadRequest)
		response.Status = utils.STATUS_ERROR
		response.Message = "Error"
	}
	ctx.JSON(response)
}

