﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PandaApp.Model;
using System.Net.Http;
using PandaApp.Common;
using System.Net.Http.Headers;
using Newtonsoft.Json;
namespace PandaApp.Controller
{
    public class ProdLineController
    {
        public String _token;
        private static readonly HttpClient _httpClient = new HttpClient();
        public ProdLineController()
        {
            _httpClient.BaseAddress = new Uri(ConfigParser.getInstance().getApiUrl());
            TimeSpan timeout = TimeSpan.FromSeconds(5);
            _httpClient.Timeout = timeout;
        }
        public ProdLineController(string token)
        {
            _httpClient.BaseAddress = new Uri(ConfigParser.getInstance().getApiUrl());
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this._token = token;
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }
        public void Dispose()
        {
            if (_httpClient != null)
                _httpClient.Dispose();
        }

        public string Login(string deviceID)
        {
            string url = "login";
            string token = null;
            Login login = new Model.Login("", "");
            login.DeviceID = deviceID;
            string jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(login);
            using (var content = new StringContent(jsonObj, Encoding.UTF8, "application/json"))
            {
                var response = _httpClient.PostAsync(url, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    string ret = response.Content.ReadAsStringAsync().Result;
                    Response<string> resMsg = JsonConvert.DeserializeObject<Response<string>>(ret);
                    if (resMsg != null)
                    {
                        token = (string)resMsg.Data;
                    }
                }
                return token;
            }
        }
        public string Login(string username, string password)
        {
            Login login = new Login(username, password);
            string url = "login";
            string token = null;
            string jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(login);
            ;
            using (var content = new StringContent(jsonObj, Encoding.UTF8, "application/json"))
            {
                var response = _httpClient.PostAsync(url, content).Result;
                if (response.IsSuccessStatusCode)
                {
                    string ret = response.Content.ReadAsStringAsync().Result;
                    Response<string> resMsg = JsonConvert.DeserializeObject<Response<string>>(ret);
                    token = (string)resMsg.Data;
                }

                return token;
            }
        }
        public void ChangeAuthorization(String token)
        {
            this._token = token;
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }
        public async Task<int> GetMaxStatusCounter(string workingDate, int factoryID, int lineID, int shiftID, int workplaceID, string statusCode)
        {
            int ret = 0;
            string url = "productiondtl/maxstatuscounter/" + workingDate + "/" + factoryID + "/" + lineID + "/" + shiftID + "/" + workplaceID + "/" + statusCode;
            using (var response = await _httpClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Response<int> resMsg = JsonConvert.DeserializeObject<Response<int>>(content);
                    ret = resMsg.Data;
                }
            }
            return ret;
        }
        public async Task<ProductionPlan> GetProductionPlan(string workingDate, int factoryID, int lineID, int shiftID, int workplaceID)
        {

            string url = "productionplan/workplace/" + workingDate + "/" + factoryID + "/" + lineID + "/" + shiftID + "/" + workplaceID;
            ProductionPlan plan = null;
            using (var response = await _httpClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Response<ProductionPlan> resMsg = JsonConvert.DeserializeObject<Response<ProductionPlan>>(content);
                    if (resMsg.Data != null)
                        plan = resMsg.Data;
                }
            }
            return plan;
        }

       
        public async Task<WorkPlace> GetWorkPlaceInfo(string factoryCode, string lineCode, int workplaceID)
        {
            WorkPlace wInfo = new WorkPlace();
            string url = "workplace/info/" + factoryCode + "/" + lineCode + "/" + workplaceID;

            using (var response = await  _httpClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    Response<WorkPlace> resMsg = JsonConvert.DeserializeObject<Response<WorkPlace>>(content);
                    if (resMsg.Data != null)
                        wInfo = resMsg.Data;
                }
            }
            return wInfo;
        }
        public async Task<List<Shift>> GetAllShift(int factoryID)
        {
            string url = "shift/" + factoryID;
            List<Shift> shiftList = null;
            using (var response = await _httpClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    Response<List<Shift>> resMsg = JsonConvert.DeserializeObject<Response<List<Shift>>>(content);
                    if (resMsg.Data != null)
                        shiftList = (List<Shift>)resMsg.Data;
                }
                return shiftList;
            }
        }



        public async Task<WorkPlaceStatus> GetCurrentWPlaceStatus(string workingDate, int factoryID, int lineID, int shiftID, int wPlaceID)
        {
            WorkPlaceStatus result = null;
            string content;
            string url = "productiondtl/workplace/" + workingDate + "/" + factoryID + "/" + lineID + "/" + shiftID + "/" + wPlaceID;
            using (var response = await _httpClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    content = await response.Content.ReadAsStringAsync();
                    Response<WorkPlaceStatus> resMsg = JsonConvert.DeserializeObject<Response<WorkPlaceStatus>>(content);
                    if (resMsg.Data != null)
                    {
                        result = resMsg.Data;
                    }
                }
            }

            return result;
        }
        public async Task<bool> CheckHealth()
        {
            using (var response = await _httpClient.GetAsync("/health"))
            {
                if (response.IsSuccessStatusCode)
                {
                    return false;
                }
                string content = await response.Content.ReadAsStringAsync();
                if (content.Equals("OK"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task<bool> NewProductionPlan(ProductionPlan obj)
        {
            var jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            using (var content = new StringContent(jsonObj))
            {
                var response = await _httpClient.PostAsync("productionplan", content);
                return response.IsSuccessStatusCode;
            }
        }


        public async void UpdateProductQty(ProductionPlan obj)
        {
            var jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            using (var content = new StringContent(jsonObj))
            {
                await  _httpClient.PutAsync("productionplan/productqty", content);
                //return response.IsSuccessStatusCode;
            }
        }
        public async void NewProductionDtl(ProductionDtl obj)
        {
            var jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            using (var content = new StringContent(jsonObj))
            {
                var response = await _httpClient.PostAsync("productiondtl", content);
                //return response.IsSuccessStatusCode;
            }
        }
        public async void UpdateWorkingStatus(ProductionDtl obj)
        {
            var jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            using (var content = new StringContent(jsonObj))
            {
                await _httpClient.PutAsync("productiondtl/stoptime", content);
                //return response.IsSuccessStatusCode;

            }
        }
        public async Task PutDoneJob(ProductionDtl obj)
        {
            var jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            using (var content = new StringContent(jsonObj))
            {
                await _httpClient.PutAsync("productiondtl", content);
                //return response.IsSuccessStatusCode;
            }
        }
        public async Task<List<Scheduler>> GetScheduler(int factoryID, int lineID, int shiftID)
        {
            List<Scheduler> result = null;
            string url = "productionplan/status/" + factoryID + "/" + lineID + "/" + shiftID;
            using (var response = await _httpClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Response<List<Scheduler>> resMsg = JsonConvert.DeserializeObject<Response<List<Scheduler>>>(content);
                    if (resMsg.Data != null)
                    {
                        result = resMsg.Data;
                    }
                }
            }
            return result;
        }
        public class Response<T>
        {
            public string Status { get; set; }
            public string Message { get; set; }
            public T Data { get; set; }

        }
    }
}
