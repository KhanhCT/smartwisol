﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaApp.Model
{
    public class WorkPlaceStatus
    {
        public string StatusCode { get; set; }
        public int StatusCounter { get; set; }
        public string StatusMsg { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime StopTime { get; set; }
        public bool Finished { set; get; }
    }
}
