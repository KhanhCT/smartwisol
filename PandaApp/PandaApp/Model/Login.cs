﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaApp.Model
{
    public class Login
    {
        string username;
        string password;
        public string DeviceID { get; set; }
        public Login(string _username, string _password)
        {
            username = _username;
            password = _password;
        }
        public Login(string deviceID)
        {
            this.DeviceID = deviceID;
        }

    }
}
