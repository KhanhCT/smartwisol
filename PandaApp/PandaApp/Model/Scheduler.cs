﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaApp.Model
{
    public class Scheduler
    {
        public Scheduler(int start, int stop)
        {
            StartTime = start;
            StopTime = stop;
        }
        public int FactoryID { get; set; }
        public int LineID { get; set; }
        public int ShiftID { get; set; }
        public int StartTime { get; set; }
        public int StopTime { get; set; }
    }
}
