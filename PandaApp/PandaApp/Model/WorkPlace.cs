﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaApp.Model
{
    public class WorkPlace
    {
        public int WorkPlaceID { get; set; }
        public int FactoryID { get; set; }
        public int LineID { get; set; }
        public int ShiftID { get; set; }
        public string Code { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        private int ProductQty { get; set; }
        public float CycleTime { get; set; }
        public int NowTarget { get; set; }
        public int NowResult { get; set; }
        public int DayTarget { get; set; }
        public bool Disabled { set; get; }
    }
}
