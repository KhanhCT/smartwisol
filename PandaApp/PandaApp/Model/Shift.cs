﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PandaApp.Model
{
    public class Shift
    {
        public int FactoryID { set; get; }
        public int ShiftID { set; get; }
        public string ShiftName { set; get; }
        public int StartTime { set; get; }
        public int StopTime { set; get; }
        public bool Disabled { set; get; }
    }
}
