﻿using System;
using System.Globalization;
namespace PandaApp.Common
{
    public class DateTimeUtils
    {
        public const string DATE = "yyyy-MM-dd";
        public const string TIME = "yyyy-MM-dd HH:mm:ss";
        public static DateTime getCurrentDate()
        {
            return DateTime.Now;
        }
        public static string compareDate(DateTime pre, DateTime next)
        {
            TimeSpan interval = next - pre;
            string msg = string.Format("{0:00}:{1:00}:{2:00}",
               (int)interval.Hours,
                    interval.Minutes,
                    interval.Seconds);
            return msg;
        }
        public static String getCurrentDate(string pattern)
        {
            return DateTime.Now.ToString(pattern);
        }
        public static String convertDate(DateTime date, string pattern)
        {
            return date.ToString(pattern);
        }

        public static DateTime parseDate(string dateValue, string pattern)
        {
            DateTime parsedDate;
            if (!DateTime.TryParseExact(dateValue, pattern, null, DateTimeStyles.None, out parsedDate))
            {
                //LOGGER.ErrorFormat("Unable to convert '{0}' to a date and time.",
                //               dateValue);
            }
            return parsedDate;
        }
        public static TimeSpan getCurrentTime()
        {
            return DateTime.Now.TimeOfDay;
        }
        public static int GetLastYear()
        {
            return DateTime.Today.AddYears(-1).Year;
        }
        public static object GetCurrentYear()
        {
            return DateTime.Now.Year;
        }

        public static bool IsCurrentYear(DateTime date)
        {
            DateTime now = DateTime.Now;
            return now.Year == date.Year;
        }
        public static bool IsCurrentMonth(DateTime date)
        {
            DateTime now = DateTime.Now;
            return IsCurrentYear(date) && now.Month == date.Month;
        }
        public static bool IsToday(DateTime date)
        {
            DateTime now = DateTime.Now;
            return IsCurrentMonth(date) && now.Day == date.Day;
        }
    }
}
