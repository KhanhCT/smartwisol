﻿namespace PandaApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lbTimeValue = new DevExpress.XtraEditors.LabelControl();
            this.lbWorkingDate = new DevExpress.XtraEditors.LabelControl();
            this.modelLabel = new DevExpress.XtraEditors.LabelControl();
            this.lbGoalValue = new DevExpress.XtraEditors.LabelControl();
            this.goalLabel = new DevExpress.XtraEditors.LabelControl();
            this.lbNRValue = new DevExpress.XtraEditors.LabelControl();
            this.nrLabel = new DevExpress.XtraEditors.LabelControl();
            this.lbNTValue = new DevExpress.XtraEditors.LabelControl();
            this.ntLabel = new DevExpress.XtraEditors.LabelControl();
            this.lbDTValue = new DevExpress.XtraEditors.LabelControl();
            this.dtLabel = new DevExpress.XtraEditors.LabelControl();
            this.lbModel = new DevExpress.XtraEditors.LabelControl();
            this.lbStatus = new DevExpress.XtraEditors.LabelControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = System.Drawing.Color.Red;
            this.panelControl1.Appearance.BorderColor = System.Drawing.Color.Lime;
            this.panelControl1.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.Appearance.Options.UseBorderColor = true;
            this.panelControl1.Appearance.Options.UseForeColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.panelControl1.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl1.ContentImage")));
            this.panelControl1.Controls.Add(this.lbTimeValue);
            this.panelControl1.Controls.Add(this.lbGoalValue);
            this.panelControl1.Controls.Add(this.lbNRValue);
            this.panelControl1.Controls.Add(this.lbNTValue);
            this.panelControl1.Controls.Add(this.lbDTValue);
            this.panelControl1.Controls.Add(this.lbModel);
            this.panelControl1.Location = new System.Drawing.Point(1314, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(573, 1052);
            this.panelControl1.TabIndex = 0;
            // 
            // lbTimeValue
            // 
            this.lbTimeValue.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.lbTimeValue.Appearance.Font = new System.Drawing.Font("Tahoma", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTimeValue.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lbTimeValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbTimeValue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTimeValue.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.lbTimeValue.Location = new System.Drawing.Point(0, 32);
            this.lbTimeValue.Name = "lbTimeValue";
            this.lbTimeValue.Size = new System.Drawing.Size(574, 170);
            this.lbTimeValue.TabIndex = 27;
            this.lbTimeValue.Text = "00:00:00";
            // 
            // lbWorkingDate
            // 
            this.lbWorkingDate.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.lbWorkingDate.Appearance.Font = new System.Drawing.Font("Tahoma", 80.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWorkingDate.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lbWorkingDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbWorkingDate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbWorkingDate.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.lbWorkingDate.Location = new System.Drawing.Point(0, 32);
            this.lbWorkingDate.Name = "lbWorkingDate";
            this.lbWorkingDate.Size = new System.Drawing.Size(673, 170);
            this.lbWorkingDate.TabIndex = 25;
            this.lbWorkingDate.Text = "15/10/2019";
            // 
            // modelLabel
            // 
            this.modelLabel.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.modelLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modelLabel.Appearance.ForeColor = System.Drawing.Color.White;
            this.modelLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.modelLabel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.modelLabel.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.modelLabel.Location = new System.Drawing.Point(0, 200);
            this.modelLabel.Name = "modelLabel";
            this.modelLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.modelLabel.Size = new System.Drawing.Size(673, 170);
            this.modelLabel.TabIndex = 23;
            this.modelLabel.Text = "MODEL";
            // 
            // lbGoalValue
            // 
            this.lbGoalValue.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.lbGoalValue.Appearance.Font = new System.Drawing.Font("Tahoma", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGoalValue.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lbGoalValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbGoalValue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbGoalValue.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.lbGoalValue.Location = new System.Drawing.Point(0, 871);
            this.lbGoalValue.Name = "lbGoalValue";
            this.lbGoalValue.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbGoalValue.Size = new System.Drawing.Size(574, 173);
            this.lbGoalValue.TabIndex = 22;
            this.lbGoalValue.Text = "100";
            // 
            // goalLabel
            // 
            this.goalLabel.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.goalLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goalLabel.Appearance.ForeColor = System.Drawing.Color.White;
            this.goalLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.goalLabel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.goalLabel.Location = new System.Drawing.Point(0, 871);
            this.goalLabel.Name = "goalLabel";
            this.goalLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.goalLabel.Size = new System.Drawing.Size(673, 173);
            this.goalLabel.TabIndex = 21;
            this.goalLabel.Text = "NOW GOAL(%)";
            // 
            // lbNRValue
            // 
            this.lbNRValue.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.lbNRValue.Appearance.Font = new System.Drawing.Font("Tahoma", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNRValue.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lbNRValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbNRValue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbNRValue.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.lbNRValue.Location = new System.Drawing.Point(0, 703);
            this.lbNRValue.Name = "lbNRValue";
            this.lbNRValue.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbNRValue.Size = new System.Drawing.Size(574, 170);
            this.lbNRValue.TabIndex = 20;
            this.lbNRValue.Text = "1000";
            // 
            // nrLabel
            // 
            this.nrLabel.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.nrLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nrLabel.Appearance.ForeColor = System.Drawing.Color.White;
            this.nrLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.nrLabel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.nrLabel.Location = new System.Drawing.Point(0, 703);
            this.nrLabel.Name = "nrLabel";
            this.nrLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.nrLabel.Size = new System.Drawing.Size(673, 170);
            this.nrLabel.TabIndex = 19;
            this.nrLabel.Text = "NOW RESULT";
            // 
            // lbNTValue
            // 
            this.lbNTValue.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.lbNTValue.Appearance.Font = new System.Drawing.Font("Tahoma", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNTValue.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lbNTValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbNTValue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbNTValue.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.lbNTValue.Location = new System.Drawing.Point(0, 536);
            this.lbNTValue.Name = "lbNTValue";
            this.lbNTValue.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbNTValue.Size = new System.Drawing.Size(574, 170);
            this.lbNTValue.TabIndex = 18;
            this.lbNTValue.Text = "10000";
            // 
            // ntLabel
            // 
            this.ntLabel.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.ntLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ntLabel.Appearance.ForeColor = System.Drawing.Color.White;
            this.ntLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.ntLabel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.ntLabel.Location = new System.Drawing.Point(0, 536);
            this.ntLabel.Name = "ntLabel";
            this.ntLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.ntLabel.Size = new System.Drawing.Size(673, 170);
            this.ntLabel.TabIndex = 17;
            this.ntLabel.Text = "NOW TARGET";
            // 
            // lbDTValue
            // 
            this.lbDTValue.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.lbDTValue.Appearance.Font = new System.Drawing.Font("Tahoma", 80.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDTValue.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lbDTValue.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbDTValue.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbDTValue.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.lbDTValue.Location = new System.Drawing.Point(0, 368);
            this.lbDTValue.Name = "lbDTValue";
            this.lbDTValue.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbDTValue.Size = new System.Drawing.Size(574, 170);
            this.lbDTValue.TabIndex = 16;
            this.lbDTValue.Text = "10000";
            // 
            // dtLabel
            // 
            this.dtLabel.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.dtLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtLabel.Appearance.ForeColor = System.Drawing.Color.White;
            this.dtLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.dtLabel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.dtLabel.Location = new System.Drawing.Point(0, 368);
            this.dtLabel.Name = "dtLabel";
            this.dtLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.dtLabel.Size = new System.Drawing.Size(673, 170);
            this.dtLabel.TabIndex = 15;
            this.dtLabel.Text = "DAY TARGET";
            // 
            // lbModel
            // 
            this.lbModel.Appearance.BorderColor = System.Drawing.Color.DarkOrange;
            this.lbModel.Appearance.Font = new System.Drawing.Font("Tahoma", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModel.Appearance.ForeColor = System.Drawing.Color.Yellow;
            this.lbModel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbModel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbModel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbModel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.lbModel.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.RightCenter;
            this.lbModel.Location = new System.Drawing.Point(0, 200);
            this.lbModel.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.lbModel.Name = "lbModel";
            this.lbModel.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.lbModel.Size = new System.Drawing.Size(574, 170);
            this.lbModel.TabIndex = 14;
            this.lbModel.Text = "SWT 1000";
            this.lbModel.Click += new System.EventHandler(this.lbModel_Click);
            // 
            // lbStatus
            // 
            this.lbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lbStatus.Appearance.Font = new System.Drawing.Font("Tahoma", 90F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lbStatus.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbStatus.Location = new System.Drawing.Point(5, 12);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(646, 1035);
            this.lbStatus.TabIndex = 12;
            this.lbStatus.Text = "RUN";
            // 
            // panelControl2
            // 
            this.panelControl2.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl2.ContentImage")));
            this.panelControl2.Controls.Add(this.lbWorkingDate);
            this.panelControl2.Controls.Add(this.modelLabel);
            this.panelControl2.Controls.Add(this.dtLabel);
            this.panelControl2.Controls.Add(this.goalLabel);
            this.panelControl2.Controls.Add(this.ntLabel);
            this.panelControl2.Controls.Add(this.nrLabel);
            this.panelControl2.Location = new System.Drawing.Point(644, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(673, 1052);
            this.panelControl2.TabIndex = 1;
            // 
            // panelControl3
            // 
            this.panelControl3.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelControl3.ContentImage")));
            this.panelControl3.Controls.Add(this.lbStatus);
            this.panelControl3.Location = new System.Drawing.Point(-5, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(651, 1052);
            this.panelControl3.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1904, 1042);
            this.Controls.Add(this.panelControl3);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Productivity";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormClosingListener);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl lbModel;
        private DevExpress.XtraEditors.LabelControl lbStatus;
        private DevExpress.XtraEditors.LabelControl dtLabel;
        private DevExpress.XtraEditors.LabelControl lbDTValue;
        private DevExpress.XtraEditors.LabelControl lbNTValue;
        private DevExpress.XtraEditors.LabelControl ntLabel;
        private DevExpress.XtraEditors.LabelControl lbGoalValue;
        private DevExpress.XtraEditors.LabelControl goalLabel;
        private DevExpress.XtraEditors.LabelControl lbNRValue;
        private DevExpress.XtraEditors.LabelControl nrLabel;
        private DevExpress.XtraEditors.LabelControl modelLabel;
        private DevExpress.XtraEditors.LabelControl lbWorkingDate;
        private DevExpress.XtraEditors.LabelControl lbTimeValue;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.PanelControl panelControl3;
    }
}

