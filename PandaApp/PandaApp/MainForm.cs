﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using PandaApp.Common;
using PandaApp.Model;
using PandaApp.Controller;
using System.Threading;
using PandaApp.GPIOCommunication;
using static PandaApp.GPIOCommunication.GPIOHelper.GPIOPin;

namespace PandaApp
{
    public partial class MainForm : Form
    {
        private WorkPlace _wPlace;
        private Shift _curShift;  
        private ProdLineController _controller;
        private WorkPlaceStatus _wPlaceStatus;
        private System.Windows.Forms.Timer _UpdateShiftTimer, _UpdateUITimer, _UpdateWorkingStatus;
        private DateTime _workingDate, _realTimeDate;
        private List<Shift> _shiftList;
        private List<Scheduler> _schedulers;
        private bool _isUpdatedShift = true, _isUpdatedNowTaget = false;

        private GPIOHelper.GPIOBoard Board1 { get; set; }
        private GPIOHelper.InputPin PIPBtn, MSBtn, NPBtn, LDBtn, CDBtn, MIPBtn, ProdCounter;
        private GPIOHelper.OutputPin LedPIPBtn, LedMSBtn, LedNPBtn, LedLDBtn, LedCDBtn, LedMIPBtn;
        public MainForm()
        {
            InitializeComponent();         
        }
        private async void MainForm_Load(object sender, EventArgs e)
        {

            await Initializer();
            await setupBoard();
            processInput();
            this.lbStatus.Ro
            this.TopMost = true;
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = FormBorderStyle.None;
        }
        private async Task  setupBoard()
        {
            string comPort = ConfigParser.getInstance().getComPort();
            try
            {
                Board1 = new GPIOHelper.GPIOBoard(0xF0, comPort);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return;
            }
            PIPBtn = Board1.InputPins[15];
            MSBtn = Board1.InputPins[14];
            NPBtn = Board1.InputPins[13];
            LDBtn = Board1.InputPins[12];
            CDBtn = Board1.InputPins[11];
            MIPBtn = Board1.InputPins[10];

            LedPIPBtn = Board1.OutputPins[14];
            LedPIPBtn.Board = this.Board1;
            LedMSBtn = Board1.OutputPins[13];
            LedMSBtn.Board = this.Board1;
            LedNPBtn = Board1.OutputPins[12];
            LedNPBtn.Board = this.Board1;
            LedLDBtn = Board1.OutputPins[11];
            LedLDBtn.Board = this.Board1;
            LedCDBtn = Board1.OutputPins[10];
            LedCDBtn.Board = this.Board1;
            LedMIPBtn = Board1.OutputPins[9];
            LedMIPBtn.Board = this.Board1;

            ProdCounter = Board1.InputPins[9];
            PIPBtn.OnPinValueChanged += OnProdInProdPinValueChanged;
            MSBtn.OnPinValueChanged += OnMatShortagePinValueChanged;
            NPBtn.OnPinValueChanged += OnNoProdPinValueChanged;
            LDBtn.OnPinValueChanged += OnLineDownPinValueChanged;
            CDBtn.OnPinValueChanged += OnChangeModelPinValueChanged;
            MIPBtn.OnPinValueChanged += OnMainInProgressPinValueChanged;
            ProdCounter.OnPinValueChanged += OnCounterPinValueChanged;
            Thread.Sleep(100);
            await Board1.GetGPIOsState();
            BlinkLedBtn(_wPlaceStatus.StatusCode);
        }

         async Task Initializer()
        {
            _realTimeDate = _workingDate = DateTime.Now;
            _controller = new ProdLineController();
            string token = _controller.Login(Utils.GetMacAddress());
            if(token == "" || token == null)
            {
                MessageBox.Show("Not get token", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return;
            }
            _controller.ChangeAuthorization(token);
            Dictionary<string, string> workPlaceInfo = ConfigParser.getInstance().getWorkPlaceInfo();
            _wPlace = await _controller.GetWorkPlaceInfo(workPlaceInfo["factory_code"], workPlaceInfo["line_code"], int.Parse(workPlaceInfo["position"]));   
            if (_wPlace == null)
            {
                MessageBox.Show("Not found workplace information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return;
            }
            _shiftList = await _controller.GetAllShift(_wPlace.FactoryID);
            if (_shiftList == null)
            {
                MessageBox.Show("Not found shift information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return;
            }
            // Finding shift
            foreach(Shift shift in _shiftList)
            {
                if(_workingDate.Hour*60 >= shift.StartTime && _workingDate.Hour * 60 < shift.StopTime)
                {
                    _curShift = shift;
                    break;
                }
            }
            if(_workingDate.Hour>=8 && _workingDate.Hour < 20)
            {
                _curShift = _shiftList[0].ShiftName.Equals("DAY") == true ? _shiftList[0] : _shiftList[1];
            }
            else
            {
                _curShift = _shiftList[0].ShiftName.Equals("NIGHT") == true ? _shiftList[0] : _shiftList[1];
                if(_workingDate.Hour > 0 && _workingDate.Hour <8)
                {
                    _workingDate = new DateTime(_workingDate.Year, _workingDate.Month, _workingDate.Day - 1);
                }
            }
            // Load Production Plan
            ProductionPlan _plan = await _controller.GetProductionPlan(DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE), _wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID, _wPlace.WorkPlaceID);
            if(_plan == null)
            {
                MessageBox.Show("Not found production plan", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return;
            }
            _wPlace.DayTarget =(int)( (12 * 60 * 60) / _wPlace.CycleTime);
            _wPlace.NowTarget = _plan.OrderedQty;
            _wPlace.NowResult = _plan.GoodProdQty;
            _wPlace.ProductID = _plan.ProductID;
            _wPlace.ProductName = _plan.ProductName;

            this.lbDTValue.Text = _wPlace.DayTarget.ToString();
            this.lbModel.Text = _plan.ProductName;
            this.lbWorkingDate.Text = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
            this.lbNTValue.Text = _wPlace.NowTarget.ToString();
            this.lbNRValue.Text = _wPlace.NowResult.ToString();
            float goal = 0.0f;
            if (_wPlace.NowTarget  !=0)
                goal = goal = ((float)_wPlace.NowResult / (float)_wPlace.NowTarget) * 100;
            this.lbGoalValue.Text = goal.ToString("0.0");

            // Get scheduler
            _schedulers = await _controller.GetScheduler(_wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID);
            if (_schedulers == null)
            {
                MessageBox.Show("Not found scheduler", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return;
            }
            // Load workplace status when running system first time
            _wPlaceStatus = await _controller.GetCurrentWPlaceStatus(DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE), _wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID, _wPlace.WorkPlaceID);
            if (_wPlaceStatus != null)
            {
                this.lbStatus.Text = _wPlaceStatus.StatusMsg;
                string _time = DateTimeUtils.compareDate(_wPlaceStatus.StartTime, _workingDate);
                this.lbTimeValue.Text = _time;
                // set color
                setColor(_wPlaceStatus.StatusCode.Trim());
            }
            // If not found any workplace status.It seems to stay NO PRODUCTION mode.
            else
            {
                // SET DEFAULT STATUS: NO PRODUCTION
                _wPlaceStatus = new WorkPlaceStatus();
                _wPlaceStatus.StatusCode = "03";
                _wPlaceStatus.StatusMsg = "NO PRODUCT";
                int counter = await _controller.GetMaxStatusCounter(DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE), _wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID, _wPlace.WorkPlaceID, _wPlaceStatus.StatusCode);
                _wPlaceStatus.StatusCounter = counter + 1;
                _wPlaceStatus.StartTime = _wPlaceStatus.StopTime = DateTime.Now;
                ProductionDtl _prodDtl = new ProductionDtl();
                _prodDtl.WorkingDate = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
                _prodDtl.FactoryID = _wPlace.FactoryID;
                _prodDtl.ShiftID = _curShift.ShiftID;
                _prodDtl.LineID = _wPlace.LineID;
                _prodDtl.WorkPlaceID = _wPlace.WorkPlaceID;
                _prodDtl.ProductID = _wPlace.ProductID;
                _prodDtl.StatusCode = _wPlaceStatus.StatusCode;
                _prodDtl.StatusCounter = _wPlaceStatus.StatusCounter;
                _prodDtl.StatusMsg = _wPlaceStatus.StatusMsg;
                try
                {
                    _controller.NewProductionDtl(_prodDtl);
                }
                catch (Exception) { }
                this.lbStatus.Text = _wPlaceStatus.StatusMsg;
                this.lbModel.Text = _wPlace.ProductName;
                this.lbDTValue.Text = _wPlace.DayTarget.ToString();
                this.lbNTValue.Text = _wPlace.NowTarget.ToString();
                this.lbNRValue.Text = "0";
                this.lbGoalValue.Text = "0.0";
                this.lbTimeValue.Text = "00:00:00";
            }

            // Create a time to update UI on real time
            _UpdateShiftTimer = new System.Windows.Forms.Timer();
            _UpdateShiftTimer.Interval = 1000;
            _UpdateShiftTimer.Tick += OnUpdateShiftEventHandler;
            _UpdateShiftTimer.Enabled = true;

            // Create a time to update UI on real time
            _UpdateUITimer = new System.Windows.Forms.Timer();
            _UpdateUITimer.Interval = 1000;
            _UpdateUITimer.Tick += OnUpdateUIEventHandler;
            _UpdateUITimer.Enabled = true;

            // Create a time to update working status on real time
            _UpdateWorkingStatus = new System.Windows.Forms.Timer();
            _UpdateWorkingStatus.Interval = 250 * 60;
            _UpdateWorkingStatus.Tick += OnUpdateWorkingStatusEventHandler;
            _UpdateWorkingStatus.Enabled = true;

        }
        // Event updating working status
        private void OnUpdateWorkingStatusEventHandler(Object source, EventArgs e)
        {
            try
            {
                // Update productivity before closing app
                if (_wPlaceStatus != null)
                {
                    // Update working status 
                    lock (this)
                    {
                        try
                        {
                            ProductionPlan _plan = new ProductionPlan();
                            _plan.WorkingDate = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
                            _plan.FactoryID = _wPlace.FactoryID;
                            _plan.LineID = _wPlace.LineID;
                            _plan.ShiftID = _curShift.ShiftID;
                            _plan.WorkingDate = _plan.WorkingDate;
                            _plan.WorkPlaceID = _wPlace.WorkPlaceID;
                            _plan.StopTime = DateTime.Now;
                            _plan.Disabled = false;
                            _plan.ProductID = _wPlace.ProductID;
                            _plan.GoodProdQty = _wPlace.NowResult;
                            _plan.OrderedQty = _wPlace.NowTarget;
                            _plan.NGProdQty = 0;
                            _controller.UpdateProductQty(_plan);

                            ProductionDtl _prodDtl = new ProductionDtl();
                            _prodDtl.WorkingDate = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
                            _prodDtl.FactoryID = _wPlace.FactoryID;
                            _prodDtl.LineID = _wPlace.LineID;
                            _prodDtl.ShiftID = _curShift.ShiftID;
                            _prodDtl.WorkPlaceID = _wPlace.WorkPlaceID;
                            _prodDtl.StatusCode = _wPlaceStatus.StatusCode;
                            _prodDtl.StatusCounter = _wPlaceStatus.StatusCounter;
                            _controller.UpdateWorkingStatus(_prodDtl);
                        }
                        catch (Exception) { }
                    }

                }
            }
            catch (Exception) { }
        }
        private void OnUpdateUIEventHandler(Object source, EventArgs e)
        {
            DateTime _now = DateTime.Now;
            // Update working date         
            // Update waiting time
            _isUpdatedNowTaget = true;
            int _offTime = _now.Hour * 60 + _now.Minute;
            foreach (Scheduler _sch in _schedulers)
            {
                if(_offTime >= _sch.StartTime && _offTime <= _sch.StopTime)
                {
                    _isUpdatedNowTaget = false; 
                    break;
                }
            }
            if (_isUpdatedNowTaget)
            {
                // Check Off Time 
                string _time = DateTimeUtils.compareDate(_wPlaceStatus.StartTime, _now);
                TimeSpan duration = _now - this._realTimeDate;
                int _nowTarget  = _wPlace.NowTarget + (int)(duration.TotalSeconds  / _wPlace.CycleTime);
                float goal = 0.0f;
                if (_nowTarget != 0)
                {
                    goal = ((float)_wPlace.NowResult / (float)_nowTarget) * 100;
                    _wPlace.NowTarget = _nowTarget;
                }              
                //UPdate UI
                this.lbWorkingDate.Text = DateTimeUtils.convertDate(_now, DateTimeUtils.DATE);
                this.lbTimeValue.Text = _time;
                this.lbGoalValue.Text = goal.ToString("0.0");
                this.lbNTValue.Text = _nowTarget.ToString();
            }
            this._realTimeDate = DateTime.Now ;
        }

        private void lbModel_Click(object sender, EventArgs e)
        {

        }

        // Timer: Update Date, Time and NowTarget each time per a second
        private async void OnUpdateShiftEventHandler(Object source, EventArgs e)
        {
            DateTime _now = DateTime.Now;
            // Change shift
            // 0: _isUpdatedShift = false, 2: _isUpdatedShift = true
            if (_now.Minute == 0 && _now.Second >= 0 && _now.Second <= 20 && (_now.Hour == 8 || _now.Hour == 20) && _isUpdatedShift)
            {
                _isUpdatedShift = false;
                await PutDoneTask();
                _workingDate = DateTime.Now;// Change working date
                _curShift = _curShift.ShiftID == _shiftList[0].ShiftID ? _shiftList[1] : _shiftList[0];
                try
                {
                    ProductionPlan _plan = await _controller.GetProductionPlan(DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE), _wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID, _wPlace.WorkPlaceID);
                    if (_plan != null)
                    {
                        _wPlace.ProductID = _plan.ProductID;
                        _wPlace.ProductName = _plan.ProductName;
                        _wPlace.ShiftID = _curShift.ShiftID;
                        _wPlace.NowResult = 0;
                        _wPlace.NowTarget = 0;
                    }
                }
                catch (Exception) { }
                // Get scheduler
                List<Scheduler> _schedulersTmp = await _controller.GetScheduler(_wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID);
                if (_schedulersTmp != null)
                {
                    _schedulers = _schedulersTmp;
                }
                _wPlaceStatus.StatusCode = "03";
                _wPlaceStatus.StatusCounter = 1;
                _wPlaceStatus.StatusMsg = "NO PRODUCT";
                _wPlaceStatus.StartTime = _wPlaceStatus.StopTime = _now;
                try
                {
                    ProductionDtl _prodDtl = new ProductionDtl();
                    _prodDtl.WorkingDate = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
                    _prodDtl.FactoryID = _wPlace.FactoryID;
                    _prodDtl.ShiftID = _curShift.ShiftID;
                    _prodDtl.LineID = _wPlace.LineID;
                    _prodDtl.WorkPlaceID = _wPlace.WorkPlaceID;
                    _prodDtl.ProductID = _wPlace.ProductID;
                    _prodDtl.StatusCode = _wPlaceStatus.StatusCode;
                    _prodDtl.StatusCounter = _wPlaceStatus.StatusCounter;
                    _prodDtl.StatusMsg = _wPlaceStatus.StatusMsg;
                    _controller.NewProductionDtl(_prodDtl);
                }
                catch (Exception) { } 
                _wPlace.NowResult = 0;
                _wPlace.NowTarget = 0;
                //_wPlaceStatus.StatusCounter += 1;
            }else if (_now.Minute == 0 &&  _now.Second > 20 && (_now.Hour == 8 || _now.Hour == 20) && !_isUpdatedShift)
            {
                _isUpdatedShift = true;
            }
        }
        // Put done a task
        private async Task PutDoneTask()
        {
            _wPlaceStatus.StopTime = DateTime.Now;
            _wPlaceStatus.Finished = true;
            try
            {
                ProductionDtl obj = new ProductionDtl();
                obj.WorkingDate = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
                obj.FactoryID = _wPlace.FactoryID;
                obj.LineID = _wPlace.LineID;
                obj.ShiftID = _curShift.ShiftID;
                obj.WorkPlaceID = _wPlace.WorkPlaceID;
                obj.ProductID = _wPlace.ProductID;
                obj.StatusCode = _wPlaceStatus.StatusCode;
                obj.StatusCounter = _wPlaceStatus.StatusCounter;
                obj.StatusMsg = _wPlaceStatus.StatusMsg;
                //obj.StartTime = DateTimeUtils.convertDate(_wPlaceStatus.StartTime, DateTimeUtils.TIME);
                //obj.StopTime = DateTimeUtils.convertDate(DateTime.Now, DateTimeUtils.TIME);
                obj.Finished = _wPlaceStatus.Finished;
                 await _controller.PutDoneJob(obj);
            }
            catch { }
        }
        private void OpenNewTask()
        {
            try
            {
                ProductionDtl _prodDtl = new ProductionDtl();
                _prodDtl.WorkingDate = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
                _prodDtl.FactoryID = _wPlace.FactoryID;
                _prodDtl.ShiftID = _curShift.ShiftID;
                _prodDtl.LineID = _wPlace.LineID;
                _prodDtl.WorkPlaceID = _wPlace.WorkPlaceID;
                _prodDtl.ProductID = _wPlace.ProductID;
                _prodDtl.StatusCode = _wPlaceStatus.StatusCode;
                _prodDtl.StatusCounter = _wPlaceStatus.StatusCounter;
                _prodDtl.StatusMsg = _wPlaceStatus.StatusMsg;
                _controller.NewProductionDtl(_prodDtl);
            }catch (Exception) { }
        }
        private void setColor(string code)
        {
            switch (_wPlaceStatus.StatusCode.Trim())
            {
                case "01":// PRODUCTION IN PROGRESS
                    {
                        this.lbStatus.ForeColor = Color.Green;
                    }
                    break;
                case "02":// MATERIAL SHORTAGE
                    {
                        this.lbStatus.ForeColor = Color.LightYellow;
                    }
                    break;
                case "03":// NO PRODUCTION
                    {
                        this.lbStatus.ForeColor = Color.White;
                    }
                    break;
                case "04":// LINE DOWN
                    {
                        this.lbStatus.ForeColor = Color.Red;
                    }
                    break;
                case "05":// CHANGE MODEL
                    {
                        this.lbStatus.ForeColor = Color.Violet;
                    }
                    break;
                case "06":// MAINTAINANCE INPROGRESS
                    {
                        this.lbStatus.ForeColor = Color.Blue;
                    }
                    break;
                default: break;
            }
        }

        private async Task changeMode(string code, string msg)
        {
            _wPlaceStatus.StatusCode = code;
            _wPlaceStatus.StatusMsg = msg;
            _wPlaceStatus.StartTime = _wPlaceStatus.StopTime = DateTime.Now;
            int counter = await _controller.GetMaxStatusCounter(DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE), _wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID, _wPlace.WorkPlaceID, _wPlaceStatus.StatusCode);
            _wPlaceStatus.StatusCounter = counter + 1;
            this.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate ()
            {
                this.lbStatus.Text = msg;
                setColor(code);
            });
        }


        async void processInput()
        {
            await Board1.GetGPIOsState();
            await Task.Run(async () =>
            {
                while (true)
                {
                    Thread.Sleep(50);
                    await Board1.GetGPIOsState();
                }
            });
        }

        public async void BlinkLedBtn(string type)
        {   
            await Board1.GetGPIOsState();
            switch (type)
            {
                case "01":
                    {
                        await LedMSBtn.RST();
                        await LedNPBtn.RST();
                        await LedLDBtn.RST();
                        await LedCDBtn.RST();
                        await LedMIPBtn.RST();
                        await LedPIPBtn.SET();
                    }
                    break;
                case "02":
                    {
                        await LedPIPBtn.RST();
                        await LedNPBtn.RST();
                        await LedLDBtn.RST();
                        await LedCDBtn.RST();
                        await LedMIPBtn.RST();
                        await LedMSBtn.SET();
                    }
                    break;
                case "03":
                    {
                        await LedPIPBtn.RST();
                        await LedMSBtn.RST();
                        await LedLDBtn.RST();
                        await LedCDBtn.RST();
                        await LedMIPBtn.RST();
                        await LedNPBtn.SET();
                    }
                    break;
                case "04":
                    {
                        await LedPIPBtn.RST();
                        await LedMSBtn.RST();
                        await LedNPBtn.RST();
                        await LedCDBtn.RST();
                        await LedMIPBtn.RST();
                        await LedLDBtn.SET();
                    }
                    break;
                case "05":
                    {
                        await LedPIPBtn.RST();
                        await LedMSBtn.RST();
                        await LedNPBtn.RST();
                        await LedLDBtn.RST();
                        await LedMIPBtn.RST();
                        await LedCDBtn.SET();
                    }
                    break;
                case "06":
                    {
                        await LedPIPBtn.RST();
                        await LedMSBtn.RST();
                        await LedNPBtn.RST();
                        await LedLDBtn.RST();
                        await LedCDBtn.RST();
                        await LedMIPBtn.SET();
                    }
                    break;
                default:
                    {
                        await LedPIPBtn.RST();
                        await LedMSBtn.RST();
                        await LedNPBtn.RST();
                        await LedLDBtn.RST();
                        await LedCDBtn.RST();
                        await LedMIPBtn.RST();
                    }
                    break;
            }
            //await Board1.GetGPIOsState();
        }

        // Handle envents when clicking buttons
        // Production In Progress
        public async void OnProdInProdPinValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if (e.Edge == GPIOHelper.Edge.Rise)
            {
                if (_wPlaceStatus != null) // save previous status before entering new status
                {
                    await PutDoneTask();
                }

                // If change model then re-load model
                if (_wPlaceStatus.StatusCode == "05")
                {                   
                    Dictionary<string, string> workPlaceInfo = ConfigParser.getInstance().getWorkPlaceInfo();
                    WorkPlace wPlaceTmp = await _controller.GetWorkPlaceInfo(workPlaceInfo["factory_code"], workPlaceInfo["line_code"], int.Parse(workPlaceInfo["position"]));
                    if (wPlaceTmp != null)
                    {
                        _wPlace = wPlaceTmp;
                    }
                    ProductionPlan _plan = await _controller.GetProductionPlan(DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE), _wPlace.FactoryID, _wPlace.LineID, _curShift.ShiftID, _wPlace.WorkPlaceID);
                    if (_plan != null)
                    {
                        _wPlace.DayTarget = (int)((12 * 60 * 60) / _wPlace.CycleTime);
                        _wPlace.ProductID = _plan.ProductID;
                        _wPlace.ProductName = _plan.ProductName;
                    }
                    _wPlace.NowResult = 0;
                    _wPlace.NowTarget = 0;
                    // Update UI
                    this.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate ()
                    {
                        
                        this.lbDTValue.Text = _wPlace.DayTarget.ToString();
                        this.lbNTValue.Text = "0";
                        this.lbNRValue.Text = "0";
                        this.lbGoalValue.Text = "0.0";
                        this.lbModel.Text = _wPlace.ProductName;
                        this.lbTimeValue.Text = "00:00:00";
                    });
                }
                await changeMode("01", "RUN");
                OpenNewTask();
                BlinkLedBtn("01");
            }
        }
        public async void OnMatShortagePinValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if (e.Edge == GPIOHelper.Edge.Rise)
            {
                // save previous status before entering new status. Put done task
                if (_wPlaceStatus != null)
                {
                    await PutDoneTask();
                }
                await changeMode("02", "WAIT MATER");
                OpenNewTask();
                BlinkLedBtn("02");
            }
        }
        
        public async void OnNoProdPinValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if (e.Edge == GPIOHelper.Edge.Rise)
            {
                // save previous status before entering new status. Put done task
                if (_wPlaceStatus != null)
                {
                    await PutDoneTask();
                }
                await changeMode("03", "NO PRODUCT");
                OpenNewTask();
                BlinkLedBtn("03");
            }
        }
        public async void OnLineDownPinValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if (e.Edge == GPIOHelper.Edge.Rise)
            {
                // save previous status before entering new status. Put done task
                if (_wPlaceStatus != null)
                {
                    await PutDoneTask();
                }
                await changeMode("04", "EQUIP DOWN");
                OpenNewTask();
                BlinkLedBtn("04");
            }
        }
        public async void OnChangeModelPinValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if (e.Edge == GPIOHelper.Edge.Rise)
            {
                // save previous status before entering new status. Put done task
                if (_wPlaceStatus != null)
                {
                    await PutDoneTask();
                }
                await changeMode("05", "CHANGE MOD");
                OpenNewTask();
                BlinkLedBtn("05");
            }
        }
        public async void OnMainInProgressPinValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if (e.Edge == GPIOHelper.Edge.Rise)
            {
                if (_wPlaceStatus != null)
                {
                    await PutDoneTask();
                }
                await changeMode("06", "PM");
                OpenNewTask();
                BlinkLedBtn("06");
            }
        }
        // Handle events when having any change in the voltage of pin
        public void OnCounterPinValueChanged(object sender, PinValueChangedEventArgs e)
        {
            if(e.Edge == GPIOHelper.Edge.Rise)
            {

                _wPlace.NowResult += 1;
                this.BeginInvoke((System.Windows.Forms.MethodInvoker)delegate ()
                {
                    this.lbNRValue.Text = _wPlace.NowResult.ToString();
                });
            }
        }
        private async void MainFormClosingListener(object sender, FormClosingEventArgs e)
        {
            try
            {
                // Update productivity before closing app
                if(_wPlaceStatus!= null && _wPlaceStatus.StatusCode.Equals("01"))
                {
                    ProductionPlan _plan = new ProductionPlan();
                    _plan.WorkingDate = DateTimeUtils.convertDate(_workingDate, DateTimeUtils.DATE);
                    _plan.FactoryID = _wPlace.FactoryID;
                    _plan.LineID = _wPlace.LineID;
                    _plan.ShiftID = _curShift.ShiftID;
                    _plan.WorkingDate = _plan.WorkingDate;
                    _plan.WorkPlaceID = _wPlace.WorkPlaceID;
                    _plan.StopTime = DateTime.Now;
                    _plan.Disabled = false;
                    _plan.ProductID = _wPlace.ProductID;
                    _plan.GoodProdQty = _wPlace.NowResult;
                    _plan.OrderedQty = _wPlace.NowTarget;
                    _plan.NGProdQty = 0;
                    _controller.UpdateProductQty(_plan);
                }
                await PutDoneTask();
            }
            catch (Exception) { }
            if (_controller != null)
                _controller.Dispose();
            if (Board1 != null)
                Board1.Close();
            if(_UpdateUITimer != null)
            {
                _UpdateUITimer.Dispose();
            }
            if(_UpdateShiftTimer != null)
            {              
                _UpdateShiftTimer.Dispose();
            }
        }
    }
}
